<?php @include('template-parts/header.php'); ?>

<section class="MyProfile Section">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-3">
				<div class="sideNavbar">
					<h2 class="userProfileNav"><span>User Profile</span> <img src="assets/img/chevron-down-white.svg" alt="dropdown_arrow"></h2>
                   <ul id="sideNav">
                     <li class="nav-item active">
                     	<svg class="icon icon-user"><use xlink:href="assets/img/cobold-sprite.svg#icon-user"></use></svg>
                       <a href="#">My Profile</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-following"><use xlink:href="assets/img/cobold-sprite.svg#icon-following"></use></svg>
                       <a href="following.php">Following</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-bookmark-count"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark-count"></use></svg>
                       <a href="savedArticles.php">Saved Articles</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-my_quotes"><use xlink:href="assets/img/cobold-sprite.svg#icon-my_quotes"></use></svg>
                       <a href="myQuotes.php">My Quotes</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-files"><use xlink:href="assets/img/cobold-sprite.svg#icon-files"></use></svg>
                       <a href="myArticles.php">My Articles</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-logout"><use xlink:href="assets/img/cobold-sprite.svg#icon-logout"></use></svg>
                       <a href="#">Logout</a>
                     </li>
                   </ul>
                </div>
			</div>

			<div class="col-lg-9">
				<div class="contentWrap">
					<div class="heading">
						<h3>My Profile</h3>
					</div>
					<div class="innerWrap">
						<form>
							<div class="form-group">
								<label for="first_name">First Name</label>
								<input type="text" name="firstName" id="first_name">
							</div>

							<div class="form-group">
								<label for="last_name">Last Name</label>
								<input type="text" name="lastName" id="last_name">
							</div>

							<div class="form-group">
								<label for="profession">Profession</label>
								<input type="text" name="profession" id="profession">
							</div>

							<div class="form-group">
								<label for="dob">Date of Birth</label>
								<input type="date" name="dob" id="dob">
							</div>

							<div class="form-group">
								<label for="gender">Gender</label>
								<select id="gender">
									<option disabled selected>Select Gender</option>
									<option>Male</option>
									<option>Female</option>
								</select>
							</div>

							<div class="form-group">
								<label for="country">Country</label>
								<select id="country">
									<option disabled selected>Select Country</option>
									<option>India</option>
									<option>UK</option>
									<option>USA</option>
								</select>
							</div>

							<div class="form-group">
								<label for="state">State</label>
								<select id="state">
									<option disabled selected>Select State</option>
									<option>A</option>
									<option>B</option>
									<option>C</option>
								</select>
							</div>

							<div class="form-group">
								<label for="city">City</label>
								<select id="city">
									<option disabled selected>Select City</option>
									<option>A</option>
									<option>B</option>
									<option>C</option>
								</select>
							</div>

							<div class="form-group">
								<label for="Bio">Bio</label>
								<input type="text" name="Bio" id="Bio">
							</div>

							<div class="form-group">
								 <div class="avatar-upload">
							        <div class="avatar-edit">
							            <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
							            <label for="imageUpload"></label>
							        </div>
							        <div class="avatar-preview">
							            <div id="imagePreview">
							            </div>
							        </div>
							    </div>
								<div class="btn-group">
									<input type="submit" name="save" value="SAVE">
									<input type="button" name="cancel" value="CACNEL">
								</div>
							</div>

						</form>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php'); ?>
	
	
