<?php @include('template-parts/header.php'); ?>
<div class="templateWrapper Section">
	<div class="container">
		<div class="headingType">
			<ul>
				<li>
					<div class="head">
						<h1>H1</h1>
					</div>
					<div class="content">
						<h1>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mi eros, semper non sodales vel erat.</h1>
					</div>
				</li>

				<li>
					<div class="head">
						<h2>H2</h2>
					</div>
					<div class="content">
						<h2>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed mi eros, semper non vestibulum in, sodales vel erat.</h2>
					</div>
				</li>

				<li>
					<div class="head">
						<h3>H3</h1>
					</div>
					<div class="content">
						<h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit, malesuada lorem sed, ullamcorper sem.</h3>

						<h3 class="light_weight">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit, malesuada lorem sed, ullamcorper sem.</h3>
					</div>
				</li>

				<li>
					<div class="head">
						<h4>H4</h4>
					</div>
					<div class="content">
						<h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit, malesuada lorem sed, ullamcorper sem.</h4>
					</div>
				</li>

				<li>
					<div class="head">
						<h5>H5</h5>
					</div>
					<div class="content">
						<h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit, malesuada lorem sed, ullamcorper sem.</h5>
					</div>
				</li>

			</ul>
		</div>

		<div class="listType">
			<div class="templateRow">
				<div class="head">Lists</div>
				<ul class="content">
					<li>Etiam ut erat interdum, semper diam in, vulputate nisi.</li>
					<li>Cras dapibus mauris eu turpis fermentum, ac ornare nisl laoreet.</li>
					<li>Pellentesque sodales sem congue tellus aliquam, ut vehicula neque facilisis.</li>
					<li>Phasellus lobortis odio sed convallis bibendum.</li>
					<li>Mauris non enim in elit tempor vehicula a at nunc.</li>
				</ul>
			</div>
		</div>

		<div class="buttonsType">
			<div class="templateRow">
			    <div class="head">Buttons</div>
				<div class="content">
					<div class="primary">
						<h3>Primary</h3>
						<a href="#" class="PinkBtn">Load More</a>
						<a href="#" class="BlackBtn">Send Article</a>
					</div>

					<div class="secondary">
						<h3>Secondary</h3>
						<a href="#" class="PinkBtn">Self & wellness</a>
						<a href="#" class="BlackBtn">Self & wellness</a>
					</div>
				</div>
			</div>
		</div>

		<div class="inputsType">
			<div class="templateRow">
			    <div class="head">Inputs</div>
				<div class="content">
					<form>
						<div class="form-group">
							<label>Full Name</label>
							<input type="text" name="" placehoder="">
						</div>

						<div class="form-group">
							<label>Email Address</label>
							<input type="email" name="" placehoder="">
						</div>
						
					</form>
				</div>
			</div>
		</div>

		<div class="inputsType">
			<div class="templateRow">
			    <div class="head">Dropdown</div>
				<div class="content">
					<form>
						<div class="form-group">
							<select>
								<option selected disabled >Select Category</option>
								<option>A</option>
								<option>B</option>
							</select>
						</div>
						<div class="form-group">
							<select>
								<option selected disabled >Select Category</option>
								<option>A</option>
								<option>B</option>
							</select>
						</div>
					</form>
				</div>
			</div>
		</div>

		<div class="pagination_wrapper">
			<div class="templateRow">
				<div class="head">Pagination</div>
				<div class="content">
					<div class="pagination">
						<ul>
							<li class="leftBtn"><a href="#"><img src="assets/img/leftarw.svg" alt="left-arrow"></a></li>
							<li><span>Page 1/100 </span></li>
							<li class="rightBtn"><a href="#"><img src="assets/img/rightarw.svg" alt="right-arrow"></a></li>
						</ul>
					</div>

					<div class="slider-arrows">
						<h3>Arrows</h3>
						<button class="arrow-prev"></button>
						<button class="arrow-next"></button>
					</div>
				</div>		
			</div>
		</div>


<!-- Home Banner Slider -->
<section class="HomeBanner innerPageSlider Section pb-0">
	<div class="container">
		<div class="wrapper">
			<div class="TopHeading">
				<h2>Slider</h2>
			</div>
			<div class="sliderRow">
				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/banner.png" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="#">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>

				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/slider-image2.jpg" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="#">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>

				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/slider-image3.jpg" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="#">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>

				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/tranding-now-img.png" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="#">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>
			</div>
	    </div>
	</div>
</section>


<section class="Section ThreeCardsWithBtmContent pb-0">
	<div class="container">
		<div class="Wrapper">
			<div class="TopHeading">
			   <h2>Component 1</h2>
			</div>
			<div class="CardsBlock">
				<div class="row">
					<div class="col-12 col-md-4">
						<article>
						<a href="#">
							<div class="imgWrap">
								<img src="assets/img/card1.png" alt="women_politics">
							</div>
							<div class="headContent">
								<div class="category">
								   <span>World &amp; Politics</span>
								</div>
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</div>
                        </a>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
								<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
							</ul>
						</div>
				     </article>
					</div>
					<div class="col-12 col-md-4">
						<article>
						<a href="#">
							<div class="imgWrap">
								<img src="assets/img/card2.png" alt="women_politics">
							</div>
							<div class="headContent">
								<div class="category">
								   <span>World &amp; Politics</span>
								</div>
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</div>
                        </a>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
								<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
							</ul>
						</div>
				     </article>
					</div>
					<div class="col-12 col-md-4">
						<article>
						<a href="#">
							<div class="imgWrap">
								<img src="assets/img/card3.png" alt="women_politics">
							</div>
							<div class="headContent">
								<div class="category">
								   <span>World &amp; Politics</span>
								</div>
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</div>
                        </a>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
								<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
							</ul>
						</div>
				     </article>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="topImagebottomContent Section">
	<div class="row">
		<div class="col-md-6">
			<div class="TopHeading">
				<h2>Component 2</h2>
			</div>
			<article>
				<a href="#">
					<div class="imgWrap">
						<img src="assets/img/latest-article-large-img.png" alt="women_politics">
					</div>
					<div class="headContent">
						<div class="category">
						   <span>World & Politics</span>
						</div>
						<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
					</div>
                </a>
				<div class="authorDetails">
					<ul>
						<li>By Shelly Mathur</li>
						<li>4 Hours Ago</li>
						<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
						<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
					</ul>
					<div class="threeDot">
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
		     </article>
		</div>

		<div class="col-md-6">
			<div class="TopHeading">
				<h2>Component 3</h2>
			</div>
			<a href="#">
			     <article class="vertical two_way_article mb-3">
					<div class="imgWrap">
						<img src="assets/img/women_politics_article.png" alt="women_politics">
					</div>
					<div class="contentWrap">
						<div class="headContent">
							<div class="category">
							   <span>World & Politics</span>
							    <ul>
									<li><span class="bookmark"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg>
									</span></li>
							        <li><span class="share"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg>
							        </span></li>
									<li>
										<div class="threeDot">
											<span></span>
											<span></span>
											<span></span>
									    </div>
								    </li>
							    </ul>
							</div>
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
						</div>
	                    
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
							</ul>
						</div>
				    </div>
			     </article>
			    </a>

			    <a href="#">
			     <article class="vertical two_way_article">
					<div class="imgWrap">
						<img src="assets/img/women_politics_article.png" alt="women_politics">
					</div>
					<div class="contentWrap">
						<div class="headContent">
							<div class="category">
							   <span>World & Politics</span>
							    <ul>
									<li><span class="bookmark"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg>
									</span></li>
							        <li><span class="share"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg>
							        </span></li>
									<li>
										<div class="threeDot">
											<span></span>
											<span></span>
											<span></span>
									    </div>
								    </li>
							    </ul>
							</div>
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
						</div>
	                    
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
							</ul>
						</div>
				    </div>
			     </article>
			    </a>
			
		</div>
	</div>
</section>

<section class="threeComponentArticles Section">
	<div class="component4">
		<div class="row">
			<div class="col-lg-4">
				<div class="TopHeading">
					<h2>Component 4</h2>
				</div>
			</div>
			<div class="col-lg-8">
				<article>
					<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in.</p>
					<div class="imgWrap">
						<img src="assets/img/singlearticle.png" alt="">
					</div>
			   </article>
		    </div>
		</div>
    </div>

    <div class="component5 Section">
		<div class="row">
			<div class="col-lg-4">
				<div class="TopHeading">
					<h2>Component 5</h2>
				</div>
			</div>
			<div class="col-lg-8">
				<article>
					<div>
					<h3>Vestibulum ex tellus, porttitor sit amet nulla nec,Curabitur efficitur massa vitae erat eleifend nunc.</h3>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit.</p>
					</div>
					<div class="imgWrap">
						<img src="assets/img/SmallRight.png" alt="">
					</div>
			   </article>
		    </div>
		</div>
    </div>

    <div class="component6">
		<div class="row">
			<div class="col-lg-4">
				<div class="TopHeading">
					<h2>Component 6</h2>
				</div>
			</div>
			<div class="col-lg-8">
				<div class="SmallRightLeftContent BorderBox">
					<div class="row">
						<div class="col-12 MobileOnly">
							<div class="ImageBox">
								<img src="assets/img/boximg.png" alt="">
							</div>
						</div>
						<div class="col-12 col-md-8">
							<div class="ContentBox">
								<h5>Test Your Knowledge of Covid- 19</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in.</p>
								<a href="#">somw.com</a>
							</div>
						</div>
						<div class="col-12 col-md-4 DesktopOnly">
							<div class="ImageBox">
								<img src="assets/img/boximg.png" alt="">
							</div>
						</div>
					</div>
				</div>
		    </div>
		</div>
    </div>
</section>



<div class="ImageMatchupHeight SingleArticleVideo Section">
	<div class="FullWidthBlock">
		<div class="TopHeading">
			<h2>Video</h2>
		</div>
		<div class="BigCenterImage">
			<img src="assets/img/videoImage.png" alt="">
			<a id="youtube-link" class="YoutubeLink" href="https://www.youtube.com/watch?v=XF7b_MNEIAg" target="">
				<img src="assets/img/play.svg" alt="">
			</a>
		</div>
	</div>
</div>

<section class="gallerySlider Section">
	<div class="MatchUpHeight">
		<div class="TopHeading">
			<h2>Gallery</h2>
		</div>
		<div class="FullWidthBlock">
			<div class="SingleArticleGallery">
			  	<img src="assets/img/galler-slide.png" />
			  	<img src="assets/img/galler-slide.png" />
			  	<img src="assets/img/galler-slide.png" />
			  	<img src="assets/img/galler-slide.png" />
			  	<img src="assets/img/galler-slide.png" />
			</div>
			<span class="slider__counter"></span>
		</div>
	</div>
</section>

	</div>
</div>

<?php @include('template-parts/footer.php'); ?>