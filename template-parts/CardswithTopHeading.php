<section class="ThreeCardsWithBtmContent Section pt-0">
	<div class="container">
		<!-- <h2>COVID 19 Desk</h2> -->
		<div class="Wrapper">
			<div class="TopHeading d-flex">
				<h2>COVID19 Desk</h2>
				<a href="">READ MORE</a>
			</div>
			<div class="row">
				<div class="col-12 col-md-4 mb-2-md">
					<article>
							<a href="single-article.php">
								<div class="imgWrap">
									<img src="assets/img/card1.png" alt="women_politics">
								</div>
							</a>
							<div class="headContent">
								<div class="category">
									<a href="#">World & Politics</a> 
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
							<div class="authorDetails">
								<ul>
									<li>By Shelly Mathur</li>
									<li>4 Hours Ago</li>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
								</ul>
							</div>
				        </article>
				</div>
				<div class="col-12 col-md-4 mb-2-md">
					<article>
							<a href="single-article.php">
								<div class="imgWrap">
									<img src="assets/img/card2.png" alt="women_politics">
								</div>
							</a>
							<div class="headContent">
								<div class="category">
									<a href="#">World & Politics</a> 
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
							<div class="authorDetails">
								<ul>
									<li>By Shelly Mathur</li>
									<li>4 Hours Ago</li>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
								</ul>
							</div>
				        </article>
				</div>
				<div class="col-12 col-md-4 mb-2-md">						
					<div class="ContriSection large">
						<div class="ArticleBox">
							<div class="flex-item">
							<h4><span>CONTRIBUTE</span> TO THIS SECTION WITH YOUR EXPERTISE</h4>
							<p>Write for us, women &amp; for yourself</p>
							</div>
							<div class="Btnbox">
								<a href="#" class="BlackBtn">Send Article</a>
							</div>
						</div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</section>