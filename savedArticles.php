<?php @include('template-parts/header.php'); ?>

<section class="MyProfile Section">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-3">
				<div class="sideNavbar">
					<h2 class="userProfileNav"><span>User Profile</span> <img src="assets/img/chevron-down-white.svg" alt="dropdown_arrow"></h2>
                   <ul id="sideNav">
                     <li class="nav-item">
                     	<svg class="icon icon-user"><use xlink:href="assets/img/cobold-sprite.svg#icon-user"></use></svg>
                       <a href="myProfile.php">My Profile</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-following"><use xlink:href="assets/img/cobold-sprite.svg#icon-following"></use></svg>
                       <a href="following.php">Following</a>
                     </li>
                     <li class="nav-item active">
                     	<svg class="icon icon-bookmark-count"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark-count"></use></svg>
                       <a href="#">Saved Articles</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-my_quotes"><use xlink:href="assets/img/cobold-sprite.svg#icon-my_quotes"></use></svg>
                       <a href="myQuotes.php">My Quotes</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-files"><use xlink:href="assets/img/cobold-sprite.svg#icon-files"></use></svg>
                       <a href="myArticles.php">My Articles</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-logout"><use xlink:href="assets/img/cobold-sprite.svg#icon-logout"></use></svg>
                       <a href="#">Logout</a>
                     </li>
                   </ul>
                </div>
			</div>

			<div class="col-lg-9">
				<div class="savedArticle">
					<div class="contentWrap">
						<div class="heading">
							<h3>Saved Articles</h3>
						</div>
						<div class="innerWrap">
							<div class="list">
								<ul>
									<li>
										<a href="#">
											<div class="leftSide">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												<div class="authorDetails">
													<span>By Shelly Mathur </span>
													<span> July 13, 2020</span>
												</div>
											</div>
											<div class="rightSide">
												<div class="imgWrap">
											<img src="assets/img/saved_article_image.png" alt="article-image">
												</div>
											</div>
										</a>
										<div class="remove">
											<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
										</div>
									</li>

									<li>
										<a href="#">
											<div class="leftSide">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												<div class="authorDetails">
													<span>By Shelly Mathur</span>
													<span>July 13, 2020</span>
												</div>
											</div>
											<div class="rightSide">
												<div class="imgWrap">
											<img src="assets/img/saved_article_image.png" alt="article-image">
												</div>
											</div>
										</a>
										<div class="remove">
											<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
										</div>
									</li>

									<li>
										<a href="#">
											<div class="leftSide">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												<div class="authorDetails">
													<span>By Shelly Mathur</span>
													<span>July 13, 2020</span>
												</div>
											</div>
											<div class="rightSide">
												<div class="imgWrap">
											<img src="assets/img/saved_article_image.png" alt="article-image">
												</div>
											</div>
										</a>
										<div class="remove">
											<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
										</div>
									</li>

									<li>
										<a href="#">
											<div class="leftSide">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												<div class="authorDetails">
													<span>By Shelly Mathur</span>
													<span>July 13, 2020</span>
												</div>
											</div>
											<div class="rightSide">
												<div class="imgWrap">
											<img src="assets/img/saved_article_image.png" alt="article-image">
												</div>
											</div>
										</a>
										<div class="remove">
											<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
										</div>
									</li>

									<li>
										<a href="#">
											<div class="leftSide">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												<div class="authorDetails">
													<span>By Shelly Mathur</span>
													<span>July 13, 2020</span>
												</div>
											</div>
											<div class="rightSide">
												<div class="imgWrap">
											<img src="assets/img/saved_article_image.png" alt="article-image">
												</div>
											</div>
										</a>
										<div class="remove">
											<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
										</div>
									</li>

									<li>
										<a href="#">
											<div class="leftSide">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												<div class="authorDetails">
													<span>By Shelly Mathur</span>
													<span>July 13, 2020</span>
												</div>
											</div>
											<div class="rightSide">
												<div class="imgWrap">
											<img src="assets/img/saved_article_image.png" alt="article-image">
												</div>
											</div>
										</a>
										<div class="remove">
											<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
			    </div>
				
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php'); ?>
	
	
