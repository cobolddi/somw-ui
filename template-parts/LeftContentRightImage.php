<section class="Section LeftContentRightImage ExtraBottomSpace GreyBgSection">
	<div class="container">
		<div class="Wrapper">
			<div class="LeftRightBox">
				<div class="RightImage MobileOnly">
					<img src="assets/img/banner.png" alt="">
				</div>
				<div class="LeftContent">
					<h2>Write For Us</h2>
					<h4>Have a great story to tell? You must write for us!</h4>
					<p>Article submission is open to all who have a story to tell and want to write it for the people of society of modern women reading from across the world.</p>
					<p>Women carry many stories with them. Stories. Written. Unwritten. Spoken. Unspoken. There are some stories of personal growth, some of inspiration, some of courage and some that are passed on to us – through influence, through memories, through experiences and some through words.</p>
					<p>SOMW publishes such stories of upliftment and influence everyday. We’re looking for original, impactful and skilfully-narrated pieces that resonate with our platform. We welcome contributors from journalists, personal writers, and anyone who has a good story to tel.</p>
				</div>
				<div class="RightImage DesktopOnly">
					<img src="assets/img/write_for_us.png" alt="">
				</div>
			</div>
		</div>
	</div>
</section>