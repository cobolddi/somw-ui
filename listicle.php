<?php @include('template-parts/header.php') ?>

<section class="Section LeftRightSticky ListiclePage" data-sticky-container>
	<div class="container">
		<div class="SingleArticle">
			<!-- <div class="LeftStickyBox DesktopOnly">
				<div class="SocialShareArticle" data-sticky data-margin-top="150" data-margin-bottom="60">
					<div class="Author">
						<a href="">
							<img src="assets/img/Photo.png" alt="">
							<p>By Shelly Mathur<br><span>July 13, 2020</span></p>
						</a>
					</div>

					<div class="SocialBlock">
						<h4>Share This Article</h4>
						<ul>
							<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
							<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
							<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
							<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
						</ul>
					</div>
				</div>
			</div> -->

			<div class="CenterFixedBox">
				<!-- <div class="SingleImg">
					<img src="assets/img/singlearticle.png" alt="">
					<p>
						Photo by Arthur Yeti on <a href="#">Unsplash</a>
					</p>
				</div> -->
				<div class="TopImageBtmContentPost">
					
					<div class="TopImage">
						<a href="" class="PostLink"><img src="assets/img/singlearticle.png" alt=""></a>
						<p>
							Photo by Arthur Yeti on <a href="#">Unsplash</a>
						</p>
					</div>
					
						<div class="BtmContent">
							<a href="" class="PostLink"><h2>5+ Moments That Made Us Go ‘I Want That Person On My Team’</h2></a>
							<div class="AuthorDetails">
								<div class="AuthorSocial MobileOnly">
									<div class="SocialBlock">
										<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/bookmark-small.svg" alt=""></a></li>
											<li class="More">
												<a href="#">
													<span></span>
													<span></span>
													<span></span>
												</a>
											</li>
										</ul>
									</div>
								</div>
								<div class="Author">
									<a href="">
										<img src="assets/img/Photo.png" alt="">
										<p>By Shelly Mathur<br><span>July 13, 2020</span></p>
									</a>
								</div>
								<div class="AuthorSocial DesktopOnly">
									<div class="SocialBlock">
										<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/bookmark-small.svg" alt=""></a></li>
											<li class="More">
												<a href="#">
													<span></span>
													<span></span>
													<span></span>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
							<p>If you have ever seen any heist movie ever, then you will know that the most important part of the movie is the part where they assemble the team! </p>
							<p>And, with this well-worn idea in mind, here are 17+ moments that made us go, “I want that person on my team!”</p>
						</div>
				</div>
				
				<div class="MobileOnly" style="margin-bottom: 3rem;">
					<div class="AdsBox" data-sticky data-sticky-for="1023" data-margin-top="150" data-margin-bottom="60">
					<a href="#"><span>Ad Space</span></a>
				</div>
				</div>

				<div class="TopImageBtmCommentBox">
					<h3>1. “The best use I’ve ever seen for false eyelashes.”</h3>
					<div class="TopImages">
						<div class="row">
							<div class="col-6 col-md-6">
								<img src="assets/img/commentimg.png" alt="">
							</div>
							<div class="col-6 col-md-6">
								<img src="assets/img/commentimg.png" alt="">
							</div>
							<div class="col-12">
								<p>
									<a href="#">Reddit | Aquagenie</a>
								</p>
							</div>
						</div>
						<!-- <img src="assets/img/Listimg1.png" alt=""> -->
						<!-- <p>
							Photo by Arthur Yeti on <a href="#">Unsplash</a>
						</p> -->
					</div>
					<div class="CommentReply">
						<p>Every team needs a tough guy, and who better to fill that very important role than this little bruiser!</p>
						<div class="CommentBox">
							<div class="HeadingFilterBox">
								<h5>0 Comments</h5>
								<div class="Filter">
									<form>
										<label for="">Sort By</label>
										<select name="" id="">
											<option value="">Oldest</option>
											<option value="">Latest</option>
											<option value="">Relevance</option>
										</select>
									</form>
								</div>
							</div>
							<div class="AddCommentBox">
								<form>
									<div class="ReplyBox">
										<img src="assets/img/commentImg.png" alt="">
										<input type="text" placeholder="Add Comments…">
									</div>
									<div class="facebookPost">
										<div class="inputGroup">
											<div class="checkboxfield">
	                                        <input type="checkbox" name="">
	                                        <label>Also post on facebook</label>
	                                        </div>
	                                        <input type="submit" name="" value="Post">
                                        </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="TopImageBtmCommentBox">
					<h3>2. “Has anyone seen Higgins?”</h3>
					<div class="TopImages">
						<!-- <div class="row">
							<div class="col-6 col-md-6">
								<img src="assets/img/commentimg.png" alt="">
							</div>
							<div class="col-6 col-md-6">
								<img src="assets/img/commentimg.png" alt="">
							</div>
							<div class="col-12">
								<p>
									Photo by Arthur Yeti on <a href="#">Unsplash</a>
								</p>
							</div>
						</div>-->						
						<img src="assets/img/Listimg1.png" alt="">
						<p>
							<a href="#">Reddit | Jaronite</a>
						</p>
					</div>
					<div class="CommentReply">
						<p>It looks like Higgins has gone rogue! I love it in a film when an agent inevitably goes rogue, like in literally any of the myriad Mission Impossible films.</p>
						<div class="CommentBox">
							<div class="HeadingFilterBox">
								<h5>0 Comments</h5>
								<div class="Filter">
									<form>
										<label for="">Sort By</label>
										<select name="" id="">
											<option value="">Oldest</option>
											<option value="">Latest</option>
											<option value="">Relevance</option>
										</select>
									</form>
								</div>
							</div>
							<div class="AddCommentBox">
								<form>
									<div class="ReplyBox">
										<img src="assets/img/commentImg.png" alt="">
										<input type="text" placeholder="Add Comments…">
									</div>
									<div class="facebookPost">
										<div class="inputGroup">
											<div class="checkboxfield">
	                                        <input type="checkbox" name="">
	                                        <label>Also post on facebook</label>
	                                        </div>
	                                        <input type="submit" name="" value="Post">
                                        </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="TopImageBtmCommentBox">
					<h3>3. “The guy is playing for both teams, and still watching it from the grandstand.”</h3>
					<div class="TopImages">
						<!-- <div class="row">
							<div class="col-6 col-md-6">
								<img src="assets/img/commentimg.png" alt="">
							</div>
							<div class="col-6 col-md-6">
								<img src="assets/img/commentimg.png" alt="">
							</div>
							<div class="col-12">
								<p>
									Photo by Arthur Yeti on <a href="#">Unsplash</a>
								</p>
							</div>
						</div>-->						
						<img src="assets/img/Listimg2.png" alt="">
						<p>
							Photo by Arthur Yeti on <a href="#">Unsplash</a>
						</p>
					</div>
					<div class="CommentReply">
						<p>It looks like Higgins has gone rogue! I love it in a film when an agent inevitably goes rogue, like in literally any of the myriad Mission Impossible films.</p>
						<div class="CommentBox">
							<div class="HeadingFilterBox">
								<h5>0 Comments</h5>
								<div class="Filter">
									<form>
										<label for="">Sort By</label>
										<select name="" id="">
											<option value="">Oldest</option>
											<option value="">Latest</option>
											<option value="">Relevance</option>
										</select>
									</form>
								</div>
							</div>
							<div class="AddCommentBox">
								<form>
									<div class="ReplyBox">
										<img src="assets/img/commentImg.png" alt="">
										<input type="text" placeholder="Add Comments…">
									</div>
									<div class="facebookPost">
										<div class="inputGroup">
											<div class="checkboxfield">
	                                        <input type="checkbox" name="">
	                                        <label>Also post on facebook</label>
	                                        </div>
	                                        <input type="submit" name="" value="Post">
                                        </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="TopImageBtmCommentBox">
					<h3>4. “This guy’s winning the lunch game.”</h3>
					<div class="TopImages">
						<!-- <div class="row">
							<div class="col-6 col-md-6">
								<img src="assets/img/commentimg.png" alt="">
							</div>
							<div class="col-6 col-md-6">
								<img src="assets/img/commentimg.png" alt="">
							</div>
							<div class="col-12">
								<p>
									Photo by Arthur Yeti on <a href="#">Unsplash</a>
								</p>
							</div>
						</div>-->						
						<img src="assets/img/Listimg3.png" alt="">
						<p>
							<a href="#">Reddit | Jaronite</a>
						</p>
					</div>
					<div class="CommentReply">
						<p>Anyone who does lunch like that is a friend of mine! I mean, I’d only really want them on my team if it was for a beer drinking team, but still!</p>
						<div class="CommentBox">
							<div class="HeadingFilterBox">
								<h5>0 Comments</h5>
								<div class="Filter">
									<form>
										<label for="">Sort By</label>
										<select name="" id="">
											<option value="">Oldest</option>
											<option value="">Latest</option>
											<option value="">Relevance</option>
										</select>
									</form>
								</div>
							</div>
							<div class="AddCommentBox">
								<form>
									<div class="ReplyBox">
										<img src="assets/img/commentImg.png" alt="">
										<input type="text" placeholder="Add Comments…">
									</div>
									<div class="facebookPost">
										<div class="inputGroup">
											<div class="checkboxfield">
	                                        <input type="checkbox" name="">
	                                        <label>Also post on facebook</label>
	                                        </div>
	                                        <input type="submit" name="" value="Post">
                                        </div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

			</div>

			<div class="RightStickyBox DesktopOnly">
				<div class="AdsBox" data-sticky data-sticky-for="1023" data-margin-top="150" data-margin-bottom="60">
					<a href="#"><span>Ad Space</span></a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="Section LeftRightSticky" data-sticky-container>
	<div class="container">
		<div class="SingleArticle">
			<div class="LeftStickyBox DesktopOnly">
				<div class="SocialShareArticle" data-sticky data-margin-top="150" data-margin-bottom="60" id="wayPointhandler">
					<div class="Author">
						<a href="#">
							<img src="assets/img/Photo.png" alt="">
							<p>By Shelly Mathur <span>July 13, 2020</span></p>
						</a>
					</div>

					<div class="SocialBlock">
						<h4>Share This Article</h4>
						<ul>
							<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
							<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
							<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
							<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="CenterFixedBox">
				<h4>World & Politics</h4>
				<h2>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h2>
				<h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in.</h5>
				<div class="MobileOnly">
					<div class="AdsBox">
						<a href="#"><span>Ad Space</span></a>
					</div>
				</div>
				<div class="AuthorDetails">
					<div class="AuthorSocial MobileOnly">
						<div class="SocialBlock">
							<ul>
								<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/bookmark-small.svg" alt=""></a></li>
								<li class="More">
									<a href="#">
										<span></span>
										<span></span>
										<span></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="Author">
						<a href="">
							<img src="assets/img/Photo.png" alt="">
							<p>By Shelly Mathur <span>July 13, 2020</span></p>
						</a>
					</div>
					<div class="AuthorSocial DesktopOnly">
						<div class="SocialBlock">
							<ul>
								<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/bookmark-small.svg" alt=""></a></li>
								<li class="More">
									<a href="#">
										<span></span>
										<span></span>
										<span></span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="SingleImg">
					<img src="assets/img/singlearticle.png" alt="">
					<p>
						Photo by Arthur Yeti on <a href="#">Unsplash</a>
					</p>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit, malesuada lorem sed, ullamcorper sem.</p>
				<p> Nulla at turpis ut tellus eleifend facilisis. Vestibulum ex tellus, porttitor sit amet nulla nec, rutrum placerat dolor. Suspendisse ipsum nisi, egestas ut dictum tempor, efficitur id tortor. Vivamus elementum efficitur ipsum, sit amet elementum mi sagittis nec. Curabitur efficitur massa vitae erat eleifend congue. Nullam a dignissim nunc, et porta risus.</p>
				<h3>Vestibulum ex tellus, porttitor sit amet nulla nec, Curabitur efficitur massa vitae erat eleifend nunc. Nullam a dignissim nunc, et porta risus.</h3>
				<p style="font-style: italic; opacity: 0.6;">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in.</p>
				<div class="orangeBullets">
					<ul>
						<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
						<li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
					</ul>
				</div>
				<div class="MatchUpHeight">
					<div class="FullWidthBlock" id="waypointOffset">
						<div class="SingleArticleGallery">
						  	<img src="assets/img/galler-slide.png" />
						  	<img src="assets/img/galler-slide.png" />
						  	<img src="assets/img/galler-slide.png" />
						  	<img src="assets/img/galler-slide.png" />
						  	<img src="assets/img/galler-slide.png" />
						</div>
						<span class="slider__counter"></span>
					</div>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit, malesuada lorem sed, ullamcorper sem.</p>
				<p>Nulla at turpis ut tellus eleifend facilisis. Vestibulum ex tellus, porttitor sit amet nulla nec, rutrum placerat dolor. Suspendisse ipsum nisi, egestas ut dictum tempor, efficitur id tortor. Vivamus elementum efficitur ipsum, sit amet elementum mi sagittis nec. Curabitur efficitur massa vitae erat eleifend congue. Nullam a dignissim nunc, et porta risus.</p>
				<div class="ImageMatchupHeight">
					<div class="FullWidthBlock">
						<div class="BigCenterImage">
							<img src="assets/img/SinglePage.png" alt="">
						</div>
					</div>
				</div>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit, malesuada lorem sed, ullamcorper sem.</p>
				<p>Nulla at turpis ut tellus eleifend facilisis. Vestibulum ex tellus, porttitor sit amet nulla nec, rutrum placerat dolor. Suspendisse ipsum nisi, egestas ut dictum tempor, efficitur id tortor. Vivamus elementum efficitur ipsum, sit amet elementum mi sagittis nec. Curabitur efficitur massa vitae erat eleifend congue. Nullam a dignissim nunc, et porta risus.</p>
				<h3>How Much Do You Know About Covid-19?</h3>
				<p style="font-style: italic; opacity: 0.6;">One positive aspect of the pandemic is that average people have gained significant knowledge about viruses and the immune system. Test your new-found virus smarts with this simple Covid-19 quiz below.</p>
				<div class="SmallRightLeftContent BorderBox">
					<div class="row">
						<div class="col-12 MobileOnly">
							<div class="ImageBox">
								<img src="assets/img/boximg.png" alt="">
							</div>
						</div>
						<div class="col-12 col-md-8">
							<div class="ContentBox">
								<h5>Test Your Knowledge of Covid- 19</h5>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in.</p>
								<a href="#">somw.com</a>
							</div>
						</div>
						<div class="col-12 col-md-4 DesktopOnly">
							<div class="ImageBox">
								<img src="assets/img/boximg.png" alt="">
							</div>
						</div>
					</div>
				</div>
				<p> Nulla at turpis ut tellus eleifend facilisis. Vestibulum ex tellus, porttitor sit amet nulla nec, rutrum placerat dolor. Suspendisse ipsum nisi, egestas ut dictum tempor, efficitur id tortor. Vivamus elementum efficitur ipsum, sit amet elementum mi sagittis nec. Curabitur efficitur massa vitae erat eleifend congue. Nullam a dignissim nunc, et porta risus.</p>

				<div class="SmallRightLeftContent">
					<div class="row">
						<div class="col-12 MobileOnly">
							<div class="ImageBox">
								<img src="assets/img/SmallRight.png" alt="">
							</div>
						</div>
						<div class="col-12 col-md-6">
							<div class="ContentBox">
								<h3>Vestibulum ex tellus, porttitor sit amet nulla nec,Curabitur efficitur massa vitae erat eleifend nunc.</h3>
								<p> Nulla at turpis ut tellus eleifend facilisis. Vestibulum ex tellus, porttitor sit amet nulla nec, rutrum placerat dolor. Suspendisse ipsum nisi, egestas ut dictum tempor, efficitur id tortor. Vivamus elementum efficitur ipsum, sit amet elementum mi sagittis nec.</p>
							</div>
						</div>
						<div class="col-12 col-md-6 DesktopOnly">
							<div class="ImageBox">
								<img src="assets/img/SmallRight.png" alt="">
							</div>
						</div>
					</div>
				</div>

				<p> Nulla at turpis ut tellus eleifend facilisis. Vestibulum ex tellus, porttitor sit amet nulla nec, rutrum placerat dolor. Suspendisse ipsum nisi, egestas ut dictum tempor, efficitur id tortor. Vivamus elementum efficitur ipsum, sit amet elementum mi sagittis nec. Curabitur efficitur massa vitae erat eleifend congue. Nullam a dignissim nunc, et porta risus.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit, malesuada lorem sed, ullamcorper sem.</p>
				<p> Nulla at turpis ut tellus eleifend facilisis. Vestibulum ex tellus, porttitor sit amet nulla nec, rutrum placerat dolor. Suspendisse ipsum nisi, egestas ut dictum tempor, efficitur id tortor. Vivamus elementum efficitur ipsum, sit amet elementum mi sagittis nec. Curabitur efficitur massa vitae erat eleifend congue. Nullam a dignissim nunc, et porta risus.</p>

				<div class="ImageMatchupHeight SingleArticleVideo">
					<div class="FullWidthBlock">
						<div class="BigCenterImage">
							<img src="assets/img/videoImage.png" alt="">
							<a id="youtube-link" class="YoutubeLink" href="https://www.youtube.com/watch?v=XF7b_MNEIAg" target="">
								<img src="assets/img/play.svg" alt="">
							</a>
						</div>
					</div>
				</div>

				<p> Nulla at turpis ut tellus eleifend facilisis. Vestibulum ex tellus, porttitor sit amet nulla nec, rutrum placerat dolor. Suspendisse ipsum nisi, egestas ut dictum tempor, efficitur id tortor. Vivamus elementum efficitur ipsum, sit amet elementum mi sagittis nec. Curabitur efficitur massa vitae erat eleifend congue. Nullam a dignissim nunc, et porta risus.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas malesuada ligula libero, eu mollis purus commodo in. Sed quis quam blandit, malesuada lorem sed, ullamcorper sem.</p>

				<div class="AuthorDetails SocialLikes">
					<div class="Author">
						<ul>
							<li><a href="#"><img src="assets/img/likes.svg" alt=""></a><span>250 Likes</span></li>
							<li><a href="#"><img src="assets/img/bookmark.svg" alt=""></a></li>
						</ul>
					</div>
					<div class="AuthorSocial">
						<div class="SocialBlock">
							<ul>
								<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="CommentsBox">
					<h2>3 Comments</h2>
					<div class="AuthorDetails">
						<div class="Author">
							<a href="">
								<img src="assets/img/Photo.png" alt="">
								<p>Arun Sharma<br><span>Oct 2019 at 2:26 AM</span></p>
							</a>
							<a href="" class="reply"><span>Reply</span></a>
						</div>
						<p>Integer tristique at nisi molestie sollicitudin. Nulla facilisi. Nulla eleifend nisi vel rhoncus semper. Nullam vel dui mattis, porttitor urna sed, sagittis quam.</p>
						<div class="AuthorDetails CommentsList">
							<div class="Author">
								<a href="">
									<img src="assets/img/Photo.png" alt="">
									<p>Arun Sharma<br><span>Oct 2019 at 2:26 AM</span></p>
								</a>
							</div>
							<p>Integer tristique at nisi molestie sollicitudin. Nulla facilisi. Nulla eleifend nisi vel rhoncus semper. Nullam vel dui mattis, porttitor urna sed, sagittis quam.</p>
						</div>
					</div>
					<div class="AuthorDetails ">
						<div class="Author">
							<a href="">
								<img src="assets/img/Photo.png" alt="">
								<p>Arun Sharma<br><span>Oct 2019 at 2:26 AM</span></p>
							</a>
							<a href="" class="reply"><span>Reply</span></a>
						</div>
						<p>Integer tristique at nisi molestie sollicitudin. Nulla facilisi. Nulla eleifend nisi vel rhoncus semper. Nullam vel dui mattis, porttitor urna sed, sagittis quam.</p>
						<div class="AuthorDetails CommentsList">
							<div class="Author">
								<a href="">
									<img src="assets/img/Photo.png" alt="">
									<p>Arun Sharma<br><span>Oct 2019 at 2:26 AM</span></p>
								</a>
							</div>
							<p>Integer tristique at nisi molestie sollicitudin. Nulla facilisi. Nulla eleifend nisi vel rhoncus semper. Nullam vel dui mattis, porttitor urna sed, sagittis quam.</p>
						</div>
					</div>
				</div>

				<div class="CommentForm ">
					<h2>Leave A Comment</h2>
					<form>
						<div class="row">
							<div class="col-12 col-md-6">
								<label>Name</label>
								<input type="text">
							</div>
							<div class="col-12 col-md-6">
								<label>Email</label>
								<input type="email">
							</div>
							<div class="col-12 col-md-12">
								<label>Message</label>
								<textarea name="" id="" cols="30" rows="10"></textarea>
							</div>
							<div class="col-12 col-md-4">
								<input type="submit" value="POST COMMENT">
							</div>
						</div>
					</form>
				</div>

			</div>
			<div class="RightStickyBox DesktopOnly">
				<div class="AdsBox" data-sticky data-sticky-for="1023" data-margin-top="150" data-margin-bottom="60">
					<a href="#"><span>Ad Space</span></a>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="ThreeCardsWithBtmContent UpdatedPostSection">
	<div class="container">
		<div class="Wrapper">
			<h2>More like this</h2>
			<div class=" Wrapper">
				<div class="CardsBlock UpdatedPost">
				  	<article>
							<a href="single-article.php">
								<div class="imgWrap">
									<img src="assets/img/card1.png" alt="women_politics">
								</div>
							</a>
							<div class="headContent">
								<div class="category">
									<a href="#">World & Politics</a> 
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
							<div class="authorDetails">
								<ul>
									<li>By Shelly Mathur</li>
									<li>4 Hours Ago</li>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
								</ul>
							</div>
				        </article>
				  	<article>
							<a href="single-article.php">
								<div class="imgWrap">
									<img src="assets/img/card2.png" alt="women_politics">
								</div>
							</a>
							<div class="headContent">
								<div class="category">
									<a href="#">World & Politics</a> 
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
							<div class="authorDetails">
								<ul>
									<li>By Shelly Mathur</li>
									<li>4 Hours Ago</li>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
								</ul>
							</div>
				        </article>
				  	<article>
							<a href="single-article.php">
								<div class="imgWrap">
									<img src="assets/img/card3.png" alt="women_politics">
								</div>
							</a>
							<div class="headContent">
								<div class="category">
									<a href="#">World & Politics</a> 
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
							<div class="authorDetails">
								<ul>
									<li>By Shelly Mathur</li>
									<li>4 Hours Ago</li>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
								</ul>
							</div>
				        </article>
				  	<article>
							<a href="single-article.php">
								<div class="imgWrap">
									<img src="assets/img/card1.png" alt="women_politics">
								</div>
							</a>
							<div class="headContent">
								<div class="category">
									<a href="#">World & Politics</a> 
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
							<div class="authorDetails">
								<ul>
									<li>By Shelly Mathur</li>
									<li>4 Hours Ago</li>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
								</ul>
							</div>
				        </article>
				  	<article>
							<a href="single-article.php">
								<div class="imgWrap">
									<img src="assets/img/card2.png" alt="women_politics">
								</div>
							</a>
							<div class="headContent">
								<div class="category">
									<a href="#">World & Politics</a> 
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
							<div class="authorDetails">
								<ul>
									<li>By Shelly Mathur</li>
									<li>4 Hours Ago</li>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
								</ul>
							</div>
				        </article>
				  	<article>
							<a href="single-article.php">
								<div class="imgWrap">
									<img src="assets/img/card3.png" alt="women_politics">
								</div>
							</a>
							<div class="headContent">
								<div class="category">
									<a href="#">World & Politics</a> 
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
							<div class="authorDetails">
								<ul>
									<li>By Shelly Mathur</li>
									<li>4 Hours Ago</li>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
								</ul>
							</div>
				        </article>
				</div>
			</div>
		</div>
	</div>
</section>



<?php @include('template-parts/footer.php') ?>