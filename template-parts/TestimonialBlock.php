
<section class="Section TestimonialsBlock">
	<div class="container">
		<div class="Wrapper">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="StaticContent">
						<h2>You have the opportunity to inspire the community of women from all around the world.</h2>
						<p>You have the opportunity to inspire the community of women from all around the world.</p>
					</div>
					<div class="BtnWrapper">
						<a href="SubmitQuote.php" class="PinkBtn">Submit your quote here</a>
					</div>
				</div>
				<div class="col-12 col-md-8">
					<div class="testinomials">
						<div class="testimonialsSlider">
						  	<div class="SlideBlock">
						  		<p>#WomenSupportingWomen is so important, we need to always remember not to compare ourselves or be jealous of someone else’s success. We all strive to be a better version of ourselves. We should always try to compliment and support each other, because not only does it make someone’s day, it makes you feel wonderful. To everyone reading this.</p>
						  		<h5>You are Amazing!</h5>
						  		<div class="Author">
						  			<img src="assets/img/icon.png" alt="">
						  			<p>Bobby Aktar<br><span>Envanto Customer</span></p>
						  		</div>
						  	</div>
						  	<div class="SlideBlock">
						  		<p>#WomenSupportingWomen is so important, we need to always remember not to compare ourselves or be jealous of someone else’s success. We all strive to be a better version of ourselves. We should always try to compliment and support each other, because not only does it make someone’s day, it makes you feel wonderful. To everyone reading this.</p>
						  		<h5>You are Amazing!</h5>
						  		<div class="Author">
						  			<img src="assets/img/icon.png" alt="">
						  			<p>Bobby Aktar<br><span>Envanto Customer</span></p>
						  		</div>
						  	</div>
						  	<div class="SlideBlock">
						  		<p>#WomenSupportingWomen is so important, we need to always remember not to compare ourselves or be jealous of someone else’s success. We all strive to be a better version of ourselves. We should always try to compliment and support each other, because not only does it make someone’s day, it makes you feel wonderful. To everyone reading this.</p>
						  		<h5>You are Amazing!</h5>
						  		<div class="Author">
						  			<img src="assets/img/icon.png" alt="">
						  			<p>Bobby Aktar<br><span>Envanto Customer</span></p>
						  		</div>
						  	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>