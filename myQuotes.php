
<?php @include('template-parts/header.php'); ?>

<section class="MyProfile Section">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-3">
				<div class="sideNavbar">
					<h2 class="userProfileNav"><span>User Profile</span> <img src="assets/img/chevron-down-white.svg" alt="dropdown_arrow"></h2>
                   <ul id="sideNav">
                     <li class="nav-item">
                     	<svg class="icon icon-user"><use xlink:href="assets/img/cobold-sprite.svg#icon-user"></use></svg>
                       <a href="myProfile.php">My Profile</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-following"><use xlink:href="assets/img/cobold-sprite.svg#icon-following"></use></svg>
                       <a href="following.php">Following</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-bookmark-count"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark-count"></use></svg>
                       <a href="savedArticles.php">Saved Articles</a>
                     </li>
                     <li class="nav-item active">
                     	<svg class="icon icon-my_quotes"><use xlink:href="assets/img/cobold-sprite.svg#icon-my_quotes"></use></svg>
                       <a href="#">My Quotes</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-files"><use xlink:href="assets/img/cobold-sprite.svg#icon-files"></use></svg>
                       <a href="myArticles.php">My Articles</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-logout"><use xlink:href="assets/img/cobold-sprite.svg#icon-logout"></use></svg>
                       <a href="#">Logout</a>
                     </li>
                   </ul>
                </div>
			</div>

			<div class="col-lg-9">
				<div class="contentWrap">
					<div class="heading">
						<h3>My Quotes</h3>
					</div>
					<div class="innerWrap">
						<div class="list">
							<ul>
								<li class="twoWaylist">
									<div class="leftSide">
										<div class="innerLeft">
											<div class="articleDetail">
												<a href="">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												</a>
												<div class="category">
													<span>Category: </span>
													<span>World & Politics</span>
												</div>
												<div class="remove">
													<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
													<a href="#"> <svg class="icon icon-edit"><use xlink:href="assets/img/cobold-sprite.svg#icon-edit"></use></svg> Edit</a>
											   </div>
											</div>
											</div>
									</div>
									<div class="rightSide">
										<h5>Under Review</h5>
										<span>July 13, 2020</span>
									</div>
									
								</li>

								<li class="twoWaylist">
									<div class="leftSide">
										<div class="innerLeft">
											<div class="articleDetail">
												<a href="">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												</a>
												<div class="category">
													<span>Category: </span>
													<span>World & Politics</span>
												</div>
												<div class="remove">
													<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
													<a href="#"> <svg class="icon icon-edit"><use xlink:href="assets/img/cobold-sprite.svg#icon-edit"></use></svg> Edit</a>
											   </div>
											</div>
											</div>
									</div>
									<div class="rightSide">
										<h5>Under Review</h5>
										<span>July 13, 2020</span>
									</div>
									
								</li>

								<li class="twoWaylist">
									<div class="leftSide">
										<div class="innerLeft">
											<div class="articleDetail">
												<a href="">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												</a>
												<div class="category">
													<span>Category: </span>
													<span>World & Politics</span>
												</div>
												<div class="remove">
													<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
													<a href="#"> <svg class="icon icon-edit"><use xlink:href="assets/img/cobold-sprite.svg#icon-edit"></use></svg> Edit</a>
											   </div>
											</div>
											</div>
									</div>
									<div class="rightSide">
										<h5>Under Review</h5>
										<span>July 13, 2020</span>
									</div>
									
								</li>

								<li class="twoWaylist">
									<div class="leftSide">
										<div class="innerLeft">
											<div class="articleDetail">
												<a href="">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												</a>
												<div class="category">
													<span>Category: </span>
													<span>World & Politics</span>
												</div>
												<div class="remove">
													<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
													<a href="#"> <svg class="icon icon-edit"><use xlink:href="assets/img/cobold-sprite.svg#icon-edit"></use></svg> Edit</a>
											   </div>
											</div>
											</div>
									</div>
									<div class="rightSide">
										<h5>Under Review</h5>
										<span>July 13, 2020</span>
									</div>
									
								</li>

								<li class="twoWaylist">
									<div class="leftSide">
										<div class="innerLeft">
											<div class="articleDetail">
												<a href="">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												</a>
												<div class="category">
													<span>Category: </span>
													<span>World & Politics</span>
												</div>
												<div class="remove">
													<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
													<a href="#"> <svg class="icon icon-edit"><use xlink:href="assets/img/cobold-sprite.svg#icon-edit"></use></svg> Edit</a>
											   </div>
											</div>
											</div>
									</div>
									<div class="rightSide">
										<h5>Under Review</h5>
										<span>July 13, 2020</span>
									</div>
									
								</li>

								<li class="twoWaylist">
									<div class="leftSide">
										<div class="innerLeft">
											<div class="articleDetail">
												<a href="">
												<h4>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h4>
												</a>
												<div class="category">
													<span>Category: </span>
													<span>World & Politics</span>
												</div>
												<div class="remove">
													<a href="#"> <svg class="icon icon-bin"><use xlink:href="assets/img/cobold-sprite.svg#icon-bin"></use></svg> Remove</a>
													<a href="#"> <svg class="icon icon-edit"><use xlink:href="assets/img/cobold-sprite.svg#icon-edit"></use></svg> Edit</a>
											   </div>
											</div>
											</div>
									</div>
									<div class="rightSide">
										<h5>Under Review</h5>
										<span>July 13, 2020</span>
									</div>
									
								</li>
							</ul>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php'); ?>
	
	
