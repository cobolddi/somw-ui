<section class="Section ExploreStories">
	<div class="container">
		<div class="Wrapper">
			<div class="storyLinkWrapper">
				<div class="StoryLink">
					<h2>Explore more stories of inspiration and trailblazing</h2>
					<p>Select the topics, subjects and blogs that interests you the most.</p>
					<ul>
						<li><a href="#">World & Politics</a></li>
						<li><a href="#">Self & Wellness</a></li>
						<li><a href="#">Arts & Culture</a></li>
						<li><a href="#">Beauty & Fitness</a></li>
						<li><a href="#">Gender & Identity</a></li>
						<li><a href="#">Science & Technology</a></li>
						<li><a href="#">Design & Fashion</a></li>
						<li><a href="#">Books & Publications</a></li>
						<li><a href="#">Artist & entertainment</a></li>
						<li><a href="#">Social & Society</a></li>
					</ul>
				</div>
			</div>
			<div class="Sponsorship">
				<div class="Headingbox">
					<h3>Sponsored</h3>
					<a href="#" target="_blank"><img src="assets/img/amazonlogo.png" alt=""></a>
				</div>
				<div class="AdBox">
					<a href="#" target="_blank"><img src="assets/img/Amazon.png" alt=""></a>
				</div>
			</div>	
		</div>
	</div>
</section>