
<!-- Home Banner Slider -->
<section class="HomeBanner innerPageSlider Section pb-0">
	<div class="container">
		<div class="wrapper">
			<div class="sliderRow">
				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/banner.png" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
										<li class="shareArticle"><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
                                        <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
										</li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="single-article.php">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>

				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/slider-image2.jpg" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="single-article.php">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>

				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/slider-image3.jpg" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="single-article.php">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>

				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/tranding-now-img.png" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="single-article.php">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>
			</div>
	    </div>
	</div>
</section>