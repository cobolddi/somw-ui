<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/HomeSlider.php') ?>

<?php @include('template-parts/RecentCardsBlogPost.php') ?>

<?php @include('template-parts/CardswithTopHeading.php') ?>

<?php @include('template-parts/TrendsBlogCards.php') ?>

<?php @include('template-parts/ExploreStories.php') ?>

<?php @include('template-parts/FollowUs.php') ?>

<?php @include('template-parts/TestimonialBlock.php') ?>

<section class="InstaFeed Section">
	<div class="container">
		<div class="TopHeading">
		    <h2>Follow Us</h2>
		</div>
		<div class="SocialIcons">
			<ul>
				<li><a href="#"><svg class="icon facebook-icon"><use xlink:href="assets/img/cobold-sprite.svg#facebook-icon"></use></svg></a></li>
				<li><a href="#"><svg class="icon icon-twitter"><use xlink:href="assets/img/cobold-sprite.svg#icon-twitter"></use></svg></a></li>
				<li><a href="#"><svg class="icon instagram-icon"><use xlink:href="assets/img/cobold-sprite.svg#instagram-icon"></use></svg></a></li>
			</ul>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-6 col-6 MBottom3">
				<img src="assets/img/card1.png" alt="">
			</div>
			<div class="col-md-4 col-sm-6 col-6 MBottom3">
				<img src="assets/img/card2.png" alt="">
			</div>
			<div class="col-md-4 col-sm-6 col-6 MBottom3">
				<img src="assets/img/card3.png" alt="">
			</div>
			<div class="col-md-4 col-sm-6 col-6 MBottom3">
			   <img src="assets/img/card1.png" alt="">
		    </div>
		    <div class="col-md-4 col-sm-6 col-6 MBottom3">
			  <img src="assets/img/card2.png" alt="">
		    </div>
		    <div class="col-md-4 col-sm-6 col-6 MBottom3">
				<img src="assets/img/card3.png" alt="">
			</div>
		</div>
		<div class="BtnWrap">
			<a href="#" class="PinkBtn">Load More</a>
		</div>
	</div>
</section>


<?php @include('template-parts/footer.php') ?>
