$(document).ready(function() {

  $('.menuBtn').click(function () {
    // $(this).append('<span></span>');
    $('.MobileMenu').addClass('act');
  });

  $('.menuOverlay').click(function (){
      $('.MobileMenu').removeClass('act');
    });
  

  $(".mainMenu").append("<span></span>");
  $(".mainMenu>span").html("&times;");
  $(".mainMenu>span").on("click",function(){
    $(this).parents("nav").parent(".MobileMenu").removeClass("act");
  });

 $(".categoryMenu>li>a").append("<span></span>");
 
$(".shareArticle").click(function(e){
   e.stopPropagation();
   e.preventDefault();
   $(this).toggleClass("active");
    $(this).find(".socialShare").toggleClass("show");
    return false;
});

$(document).click( function(){
  $(".shareArticle").removeClass("active");
    $('.socialShare').removeClass("show");
});


// BookMark Icon toggle
$(".bookmarkArticle").click(function(e){
  e.preventDefault();
  $(this).toggleClass("toggleMark");
});

// Threedot toggle dropdown
$(".threeDot").click(function(e){
   e.stopPropagation();
   e.preventDefault();
   $(this).toggleClass("active");
    $(this).find(".dropdownElement").toggleClass("show");
    return false;
});

$(document).click( function(){
  $('.threeDot').removeClass("active");
    $('.dropdownElement').removeClass("show");
});

// Header Submenu Open
// SubMenu ON and Off
  $(".submenu>a").click(function(e){
    e.preventDefault();
    $(this).parent().toggleClass("openSubMenu");
  });
  $(".submenu>ul").append("<span></span>");
  $(".submenu>ul>span").html("&times;");
  $(".submenu>ul>span").click(function(){
    $(this).parents("ul").parent("li").removeClass("openSubMenu");
  });

 // $(".submenu").find(".subNavWrap>span").on('click',function(){
 //   alert("hello");
 // });

 $(".mainMenu .categoryMenu >li a>span").click(function(e){
  e.preventDefault();
  $(this).toggleClass("spanRotate");
 $(this).parents('a').parent().toggleClass("Active_MobileDropdown");
 });



  // Fixed Header on scroll for Desktop
    window.onscroll = function() {scrollFunction()};

    function scrollFunction() { 
      if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
        document.querySelector("header.DesktopOnly").classList.add("stickyHeader");
        document.querySelector("header.MobileOnly").classList.add("stickyHeader");
      } else {
        document.querySelector("header.DesktopOnly").classList.remove("stickyHeader");
        document.querySelector("header.MobileOnly").classList.remove("stickyHeader");
      }
    }

    
    // Get Header height to make it equal to margin scrollTop
     var headerHeight = $("#header").innerHeight();
     $(".shrink").css("margin-top",headerHeight);


     var sticky = new Sticky('[data-sticky]', {});

     // Dark and light theme toggle Switch
     $(".switch .slider").on("click", function(){
        $("body").toggleClass("themeChanger");
     });

  
   // Sign In Magnific Popup
    $('.signInBtn').magnificPopup({
        type: 'inline',
        mainClass: 'mfp-with-zoom',
        zoom: {
         enabled: true, 
         duration: 500,
         easing: 'ease-in-out'
        }
    });

    // Profile Progress Magnific Popup
    $('.profilePopup').magnificPopup({
        type: 'inline',
        mainClass: 'mfp-with-zoom',
        zoom: {
         enabled: true, 
         duration: 500,
         easing: 'ease-in-out'
        }
    });

    // Search Bar Magnific Pop up
    $('header ul>li a[href="#searchBar"]').magnificPopup({
        type: 'inline',
        mainClass: 'mfp-with-zoom',
        zoom: {
         enabled: true, 
         duration: 500,
         easing: 'ease-in-out'
        }
    });

  $('#youtube-link').magnificPopup({
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,
      fixedContentPos: false,
  // Changes iFrame to support Youtube state changes (so we can close the video when it ends)
      iframe: {
        markup: '<div class="mfp-iframe-scaler">' +
          '<div class="mfp-close"></div>' +
          '<iframe id="player" class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
          '</div>', // HTML markup of popup, `mfp-close` will be replaced by the close button
  // Converts Youtube links to embeded videos in Magnific popup.
        patterns: {
          youtube: {
            index: 'youtube.com/',
            id: 'v=',
            src: '//www.youtube.com/embed/%id%?autoplay=1&rel=0&showinfo=0&enablejsapi=1'
          }
        }
      },
  // Tracks Youtube video state changes (so we can close the video when it ends)
      callbacks: {
        open: function() {
          new YT.Player('player', {
            events: {
              'onStateChange': onPlayerStateChange
            }
          });
        }
      }
    });
  

  // Slider Left Image right Content
  $('.sliderRow').slick({
      arrows: true,
      centerPadding: "0px",
      dots: false,
      slidesToShow: 1,
      infinite: false,
      adaptiveHeight: true,
      fade: true,
      cssEase: 'linear'
  });

  $('.UpdatedPost').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: true,
        responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 575,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    ]
    });
  

  $('.testimonialsSlider').slick({
        infinite: true,
        // fade: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 2000,
        arrows: false,
        dots: true,
        responsive: [{
          breakpoint: 600,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
           breakpoint: 400,
           settings: {
              arrows: false,
              slidesToShow: 1,
              slidesToScroll: 1
           }
        }]
    });

    var $HomeSliderNo = $('.slider__counter');
    var $HomeslickElement = $('.SingleArticleGallery');

    $HomeslickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
      //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
      var i = (currentSlide ? currentSlide : 0) + 1;
      $HomeSliderNo.html('0' + i + ' / ' + '0' + slick.slideCount);
    });

    $('.SingleArticleGallery').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        responsive: [{
          breakpoint: 600,
          settings: {
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
           breakpoint: 400,
           settings: {
              arrows: true,
              slidesToShow: 1,
              slidesToScroll: 1
           }
        }]
    });

    // User Image upload with preview
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url('+e.target.result +')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(500);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imageUpload").change(function() {
        readURL(this);
    });
   
   $(".userProfileNav").click(function(){
     $("#sideNav").slideToggle();
   });

// footer form email subscription
   $(".es_submit_button").attr("value","");

// Waypoint js fade in Fade out
$('#waypointOffset').waypoint(function(direction) {
    
    if (direction ==='down') {
            $("#wayPointhandler").fadeTo(400, 0);
        }
        else {
                $("#wayPointhandler").fadeTo(400, 1);        
        }
},{
    offset: 400
    });

// $('#waypointOffset').waypoint(function(direction) {
    
//     if (direction ==='down') {
//             $("#wayPointhandler").fadeTo(1000, 1);
//         }
//         else {
//                 $("#wayPointhandler").fadeTo(1000, 0.2);        
//         }
// },{
//     offset: 400
//     });

// });


})
