<?php @include('template-parts/header.php') ?>


<section class="ContactUsPage SubmitQuote Section">
	<div class="container">
		<div class="TopHeading">
			<h2>Submit A Quote</h2>
			<p>You have the opportunity to inspire the community of women from all around the world.</p>
			<p>Have something to say? Or a favorite inspirational quote you'd like to share?</p>
		</div>

		<div class="whiteBoxForm">	
				<form action="">
					<div class="row">
						<div class="col-12 col-md-6 MBottom2">
							<label>Name</label>
							<input type="text">
						</div>
						<div class="col-12 col-md-6 MBottom2">
							<label>Emaill address</label>
							<input type="email">
						</div>
						<div class="col-12 col-md-6 MBottom2">
							<label>Category</label>
							<select name="" id="">
								<option selected disabled>Select Category</option>
								<option value="">A</option>
								<option value="">B</option>
							</select>
						</div>
						<div class="col-12 col-md-6 MBottom2">
							<label>Your Country</label>
							<select name="" id="">
								<option selected disabled>Select Country</option>
								<option value="">A</option>
								<option value="">B</option>
							</select>
						</div>
						<div class="col-12 col-md-12 MBottom2">
							<label>Your Quote (Max 500 words)</label>
							<textarea></textarea>
						</div>
						<div class="col-12 col-md-3">
							<input type="submit" value="Submit Quote">
						</div>
					</div>
				</form>
			</div>
	    </div>
	</div>
</section>

<?php @include('template-parts/footer.php') ?>
