
</main>

<footer>
	<div class="TopFooter">
		<div class="container">
			<div class="LogoBox">
				<div class="row">
					<div class="col-12 col-md-6">
						<a href="#" class="Footerlogo"><img src="assets/img/footerlogo.svg" alt=""></a>
					</div>
					<div class="col-12 col-md-6">
						<div class="FormBlock">
							<h4>Never Miss an Outside Story</h4>
							<form>
								<input type="email" placeholder="Enter E-mail Address">
							    <input type="submit" value="">
							</form>
						</div>
					</div>
				</div>
				<div class="DesktopOnly MTop5">
					<ul>
						<li><a href="#">About</a></li>
							<li><a href="guideLine.php">Write for us</a></li>
							<li><a href="ContactUs.php">Join the Society</a></li>
							<li><a href="#signInPopUp" class="signInBtn">Login</a></li>
					</ul>
					<ul>
						<li><a href="category-page.php">World & Politics</a></li>
						<li><a href="#">Self & Wellness</a></li>
						<li><a href="#">Arts & Culture</a></li>
						<li><a href="#">Beauty</a></li>
						<li><a href="#">Cuisine & Health</a></li>
					</ul>
				</div>
			</div>
	    </div>
		<div class="MobileOnly">
			<div class="SocialBlock">
				<ul>
					<li><p>Follow us :</p></li>
					<li><a href="#"><img src="assets/img/facebook.svg" alt=""></a></li>
					<li><a href="#"><img src="assets/img/twitter.svg" alt=""></a></li>
					<li><a href="#"><img src="assets/img/insta.svg" alt=""></a></li>
					<li><a href="#"><img src="assets/img/linkedin.svg" alt=""></a></li>
				</ul>
			</div>
			<div class="BottomFooter">
				<div class="Copyright">
					<p>Copyright 2020, Society of Modern Women</p>
				</div>
				<div class="Deliveryby">
					<p>Delivered by <a href="cobold.co">Cobold Digital</a></p>
				</div>
			</div>
		</div>
	</div>
	<div class="BottomFooter DesktopOnly">
		<div class="container">
			<div class="Wrapper">
				<div class="row align-center">
					<div class="col-12 col-md-4">
						<div class="Copyright">
							<p>Copyright 2020, Society of Modern Women</p>
						</div>
					</div>
					<div class="col-12 col-md-4 DesktopOnly">
						<div class="SocialBlock">
							<ul>
								<li><p>Follow us :</p></li>
								<li><a href="#"><img src="assets/img/facebook.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/twitter.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/insta.svg" alt=""></a></li>
								<li><a href="#"><img src="assets/img/linkedin.svg" alt=""></a></li>
							</ul>
						</div>
					</div>
					<div class="col-12 col-md-4">
						<div class="Deliveryby">
							<p>Delivered by <a href="cobold.co">Cobold Digital</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>

<!-- Sign in popup -->
<div class="popup_wrapper mfp-hide" id="signInPopUp">	
	<div class="popup_inner">
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		<img src="assets/img/logo-icon.png" alt="logo-icon">
		<h2>Sign In With</h2>
		<p>Welcome! Create a free account to get access to great stories about women and the world, that matters.</p>
		<div class="SocialSignIn">
			<a href="#" target="_blank">
				<img src="assets/img/google-large.png" alt="google-sign">
				<span>Continue with Google</span>
			</a>
			<a href="#" target="_blank">
				<img src="assets/img/facebook-blue.png" alt="facebook-sign">
				<span>Continue with Facebook</span>
			</a>
		</div>
			<p>Click “Sign In” to agree to our <a href="#">Terms & Conditions</a> and for acknowledging our <a href="#">Privacy Policy</a></p>
	</div>
</div>

<!-- popup for complete Profile -->
<div class="popup_wrapper mfp-hide" id="ProfileProgress">	
	<div class="popup_inner profile_status">
		<button title="Close (Esc)" type="button" class="mfp-close">×</button>
		<div class="innerContent">
			<div class="visitProfile">	
			<h2>Your Society Is Waiting <br> Complete Your Profile</h2>
			<a href="myProfile.php" class="PinkBtn">visit profile</a>
			</div>
			<div class="profile_progress">
				<img src="assets/img/profile_icon.png" alt="profile_icon">
				<div class="percentage">60%</div>
			</div>
		</div>
	</div>
</div>

<!-- popup for complete Profile -->
<div class="popup_wrapper mfp-hide" id="searchBar">	
	<div class="searchBarWrap">
		<form>
			<input type="search" name="searchBar" placeholder="Search Blogs" autofocus="true">
			<!-- <svg class="icon icon-search"><use xlink:href="assets/img/cobold-sprite.svg#icon-search"></use></svg> -->
		<input type="submit" name="" value="search">
		</form>
	</div>
</div>

<script src="assets/js/vendor.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>