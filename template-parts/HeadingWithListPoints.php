<section class="HeadingWithListPoints">
	<div class="container">
		<div class="ContentWithHeading BorderBox">
			<h2>Okay, what sort of stories do we expect?</h2>
			<p>While we will allow our writers, whether in-house or contributors, to rely on their own style while telling stories, all SOMW stories share the following traits: </p>
			<ul>
				<li>They are fact-based, well studied, purpose driven, with a sharp point of view.</li>
				<li>We require evidence for every claim made in the story.</li>
				<li>The idea is to also look beyond the past and present, and tell readers where things are headed. </li>
				<li>Our stories are not limited to the written word.</li>
				<li>Our preffered story length is 1500-2200 words.</li>
			</ul>
		</div>
		<div class="Section ContentWithHeading">
			<h2>Some tips on getting your articles published:</h2>
			<p>While we will allow our writers, whether in-house or contributors, to rely on their own style while telling stories, all SOMW stories share the following traits: </p>
			<ul>
				<li>Authors are welcome to submit photos, charts, graphs, and other visuals to accompany the article, which will be used at the sole discretion of the editor.</li>
				<li>Photos should be high resolution and accompanied by title and a small description.</li>
				<li>Articles should include the author’s name, title, e-mail address, and social profile for feature.</li>
				<li>Upon acceptance, the article becomes the property of SOMW and is subject to copyright ownership by SOMW.</li>
				<li>Writers are welcome to submit an outline or rough draft of the article to the managing editor for feedback. Any “pre-approval” by one editor does not guarantee that the final article will be accepted for publication.</li>
				<li>Editors may need to have reasonable edits done before publishing your story and for that may contact the author for review and approval of substantive changes to the article.</li>
				<li>Once the article is published, authors will receive the copy of the published story via email.</li>
			</ul>
			</ul>
		</div>
		<div class="whiteBoxForm guidelineForm">
			<div class="TopHeading">
			  <h2>Okay, what sort of stories do we expect?</h2>
			</div>
			<div class="FormBox">
				<form action="">
					<div class="row">
						<div class="col-12 col-lg-4 col-md-6">
							<label>Full Name</label>
							<input type="text">
						</div>

						<div class="col-12 col-lg-4 col-md-6">
							<label>Your Country</label>
							<select name="" id="">
								<option selected disabled>Select Country</option>
								<option value="">A</option>
								<option value="">B</option>
							</select>
						</div>
						<div class="col-12 col-lg-4 col-md-6">
							<label>Emaill address</label>
							<input type="email">
						</div>
						<div class="col-12 col-lg-8 col-md-6">
							<label>Story Subject</label>
							<input type="text">
						</div>
						<div class="col-12 col-lg-4 col-md-6">
							<label>Category</label>
							<select name="" id="">
								<option selected disabled>Select Category</option>
								<option value="">A</option>
								<option value="">B</option>
							</select>
						</div>
						<div class="col-12 col-lg-8 col-md-6">
							<label>Story Abstract (Max. 1000 words)</label>
							<textarea></textarea>
						</div>
						<div class="col-12 col-lg-4 alignSelfCenter">
							<div class="UploadFile">
								<label>
									Browse Files
									<input type="file">
								</label>
								<span>format .doc or pdf, max size: 30mb</span>
							</div>
						</div>
						<div class="col-12 col-md-4">
								<input type="submit" value="submit">
						</div>
						<div class="col-12 col-md-8">
							<div class="Note">
								<p><span>Note :</span> If you encounter any problems during online submission, please email your story to <a href="mailto:submit@somw.com">submit@somw.com</a></p>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>