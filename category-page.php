<?php @include('template-parts/header.php'); ?>

<!-- Latest Article Section -->
<section class="LatestArticle">
	<div class="container">
		<div class="TopHeading">
			<span>Browsing History</span>
			<h2>World & Politics</h2>
			<p>All the latest insights about Women in poolitics. A complete collection of articles and commentary on women in leadership & politics from around the world.</p>
		</div>
		<div class="Wrapper">
			<div class="row">
				<div class="col-12 col-lg-6 mb-3">
					<article>
						<a href="#">
							<div class="imgWrap">
								<img src="assets/img/latest-article-large-img.png" alt="women_politics">
							</div>
						</a>
							<div class="headContent">
								<div class="category">
									<a href="#">World & Politics</a>
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
                        
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
								<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
                                <div class="socialShare">
                                 	<ul>
										<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
									</ul>
                                 </div>
								</li>
							</ul>
							<div class="threeDot">
								<span></span>
								<span></span>
								<span></span>
								<div class="dropdownElement">
									<ul>
										<li><a href="#">Dismiss this story</a></li>
										<li><a href="#">Mute this author</a></li>
										<li><a href="#">Mute this publication</a></li>
									</ul>
								</div>
							</div>
						</div>
				     </article>
				</div>

				<div class="col-12 col-lg-3 mb-3">
					<article class="vertical">
						<a href="#">
							<div class="imgWrap">
								<img src="assets/img/latest-article-small-img.png" alt="women_politics">
							</div>
						</a>
							<div class="headContent">
								<div class="category">
									<a href="#">
									   World & Politics
									</a>
								    <ul>
										<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
										<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
		                                <div class="socialShare">
		                                 	<ul>
												<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
												<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
												<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
												<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
											</ul>
		                                 </div>
										</li>
										<li>
											<div class="threeDot">
												<span></span>
												<span></span>
												<span></span>
												<div class="dropdownElement">
													<ul>
														<li><a href="#">Dismiss this story</a></li>
														<li><a href="#">Mute this author</a></li>
														<li><a href="#">Mute this publication</a></li>
													</ul>
												</div>
											</div>
										</li>
								    </ul>
								</div>
								<a href="#">
								<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
								</a>
							</div>
							<div class="authorDetails">
								<ul>
									<li>By Shelly Mathur</li>
									<li>4 Hours Ago</li>
								</ul>
							</div>
				     </article>
				</div>

				<div class="col-12 col-lg-3 mb-3">
					<div class="advertSpace">
						Ad Space
					</div>

					<div class="ContriSection">
						<div class="ArticleBox">
							<div class="flex-item">
							<h4><span>CONTRIBUTE</span> TO THIS SECTION WITH YOUR EXPERTISE</h4>
							<p>Write for us, women &amp; for yourself</p>
							</div>
							<div class="Btnbox">
								<a href="#" class="BlackBtn">Send Article</a>
							</div>
						</div>
				    </div>
				</div>
			</div>

			<div class="row">
				<div class="col-12 col-lg-4 col-md-6 mb-3">
					<article>
						<a href="single-article.php">
							<div class="imgWrap">
								<img src="assets/img/card1.png" alt="women_politics">
							</div>
						</a>
						<div class="headContent">
							<div class="category">
								<a href="#">World & Politics</a> 
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
								<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
                                <div class="socialShare">
                                 	<ul>
										<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
									</ul>
                                 </div>
								</li>
							</ul>
						</div>
			        </article>
				</div>

				<div class="col-12 col-lg-4 col-md-6 mb-3">
					<article>
						<a href="single-article.php">
							<div class="imgWrap">
								<img src="assets/img/card2.png" alt="women_politics">
							</div>
						</a>
						<div class="headContent">
							<div class="category">
								<a href="#">World & Politics</a> 
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
								<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
                                <div class="socialShare">
                                 	<ul>
										<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
									</ul>
                                 </div>
								</li>
							</ul>
						</div>
			        </article>
				</div>

				<div class="col-12 col-lg-4 col-md-6 mb-3">
					<article>
						<a href="single-article.php">
							<div class="imgWrap">
								<img src="assets/img/card2.png" alt="women_politics">
							</div>
						</a>
						<div class="headContent">
							<div class="category">
								<a href="#">World & Politics</a> 
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
								<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
                                <div class="socialShare">
                                 	<ul>
										<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
									</ul>
                                 </div>
								</li>
							</ul>
						</div>
			        </article>
				</div>

				<div class="col-12 col-lg-4 col-md-6 mb-3">
					<article>
						<a href="single-article.php">
							<div class="imgWrap">
								<img src="assets/img/card1.png" alt="women_politics">
							</div>
						</a>
						<div class="headContent">
							<div class="category">
								<a href="#">World & Politics</a> 
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
								<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
                                <div class="socialShare">
                                 	<ul>
										<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
									</ul>
                                 </div>
								</li>
							</ul>
						</div>
			        </article>
				</div>

				<div class="col-12 col-lg-4 col-md-6 mb-3">
					<article>
						<a href="single-article.php">
							<div class="imgWrap">
								<img src="assets/img/card2.png" alt="women_politics">
							</div>
						</a>
						<div class="headContent">
							<div class="category">
								<a href="#">World & Politics</a> 
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
								<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
                                <div class="socialShare">
                                 	<ul>
										<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
									</ul>
                                 </div>
								</li>
							</ul>
						</div>
			        </article>
				</div>

				<div class="col-12 col-lg-4 col-md-6 mb-3">
					<article>
						<a href="single-article.php">
							<div class="imgWrap">
								<img src="assets/img/card3.png" alt="women_politics">
							</div>
						</a>
						<div class="headContent">
							<div class="category">
								<a href="#">World & Politics</a> 
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
								<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
								<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
                                <div class="socialShare">
                                 	<ul>
										<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
										<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
									</ul>
                                 </div>
								</li>
							</ul>
						</div>
			        </article>
				</div>
			</div>
        </div>
	</div>
</section>
     
 <!-- Inside Banner -->
<section class="innerPageSlider trandingSlider Section pb-0">
	<div class="container">
		<div class="TopHeading">
			<h2>Trending Now</h2>
		</div>
		<div class="wrapper">
			<!-- <h2 class="Verticaltext">Trending Now</h2> -->
			<div class="sliderRow">
				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/tranding-now-img.png" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
										<li class="shareArticle"><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
                                        <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
										</li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="#">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>

				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/slider-image2.jpg" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="#">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>

				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/slider-image3.jpg" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="#">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>

				<div class="singleSlide">
					<div class="innerWrap">
						<div class="ImageBlock">
							<img src="assets/img/tranding-now-img.png" alt="">
						</div>
						<div class="ContentBlock">
							<div>
								<div class="topBar">
									<ul>
										<li>Featured</li>
										<li>20th April 2020</li>
										<li><a href="#"><svg class="icon icon-bookmark"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark"></use></svg></a></li>
										<li><a href="#"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a></li>
									</ul>
								</div>
								<div class="headingContent">
									<h3>The girl who refused to be a princess</h3>
									<p>We dive into the inspiring story of a real life princess who turned down royalty to lead a normal life with her prince.</p>
								</div>
						    </div>

							<div class="bottomContent">
								<h4>
									<a href="#">Read More</a>
								</h4>
								<small>WORLD POLITICS</small>
							</div>
						</div>
					</div>
				</div>
			</div>
	    </div>
	</div>
</section>
    
<!-- Testimonials -->
<section class="Section TestimonialsBlock pb-0">
	<div class="container">
		<div class="Wrapper">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="StaticContent">
						<h2>You have the opportunity to inspire the community of women from all around the world.</h2>
						<p>You have the opportunity to inspire the community of women from all around the world.</p>
					</div>
					<div class="BtnWrapper">
						<a href="#" class="PinkBtn">Submit your quote here</a>
					</div>
				</div>
				<div class="col-12 col-md-8">
					<div class="testinomials">
						<div class="testimonialsSlider">
						  	<div class="SlideBlock">
						  		<p>#WomenSupportingWomen is so important, we need to always remember not to compare ourselves or be jealous of someone else’s success. We all strive to be a better version of ourselves. We should always try to compliment and support each other, because not only does it make someone’s day, it makes you feel wonderful. To everyone reading this.</p>
						  		<h5>You are Amazing!</h5>
						  		<div class="Author">
						  			<img src="assets/img/icon.png" alt="">
						  			<p>Bobby Aktar<br><span>Envanto Customer</span></p>
						  		</div>
						  	</div>
						  	<div class="SlideBlock">
						  		<p>#WomenSupportingWomen is so important, we need to always remember not to compare ourselves or be jealous of someone else’s success. We all strive to be a better version of ourselves. We should always try to compliment and support each other, because not only does it make someone’s day, it makes you feel wonderful. To everyone reading this.</p>
						  		<h5>You are Amazing!</h5>
						  		<div class="Author">
						  			<img src="assets/img/icon.png" alt="">
						  			<p>Bobby Aktar<br><span>Envanto Customer</span></p>
						  		</div>
						  	</div>
						  	<div class="SlideBlock">
						  		<p>#WomenSupportingWomen is so important, we need to always remember not to compare ourselves or be jealous of someone else’s success. We all strive to be a better version of ourselves. We should always try to compliment and support each other, because not only does it make someone’s day, it makes you feel wonderful. To everyone reading this.</p>
						  		<h5>You are Amazing!</h5>
						  		<div class="Author">
						  			<img src="assets/img/icon.png" alt="">
						  			<p>Bobby Aktar<br><span>Envanto Customer</span></p>
						  		</div>
						  	</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
   
<!-- Follow us  -->
<section class="LeftContentRightAdvert Section pb-0">
	<div class="container">
		<div class="row">
			<div class="col-md-9 FollowUs mb-2-md">
				<div class="Wrapper">
			        <div class="HeadingBox">
						<div class="TopHeading">
						 <h2>Follow us</h2>
						</div>
						<div>
						<h5>Never miss another great story!</h5>
						<p>Get notified of the best stories on the internet.</p>
						</div>
					</div>
				
					<ul>
						<li><a href="#"><img src="assets/img/facebook.svg" alt=""><br>Facebook</a></li>
						<li><a href="#"><img src="assets/img/twitter.svg" alt=""><br>Twitter</a></li>
						<li><a href="#"><img src="assets/img/insta.svg" alt=""><br>Instagram</a></li>
						<li><a href="#"><img src="assets/img/linkedin.svg" alt=""><br>Linkedin</a></li>
					</ul>
				</div>
			</div>
			
			<div class="col-md-3 col-12">
				<div class="Sponsorship text-center">
					<div class="Headingbox">
						<h3>Sponsored</h3>
						<a href="#" target="_blank"><img src="assets/img/amazonlogo.png" alt=""></a>
					</div>
					<div class="AdBox">
						<a href="#" target="_blank"><img src="assets/img/Amazon.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>	
	</div>
</section>

<!-- Stories of women in world & politics -->
<section class="TwoWaysArticle Section pb-0">
	<div class="container">
		<div class="TopHeading">
			<h2>More Stories of Women in world & Politics</h2>
		</div>
		<div class="row">
			<div class="col-lg-6 mb-3">
			     <article class="vertical two_way_article">
			     	<a href="#">
						<div class="imgWrap">
							<img src="assets/img/women_politics_article.png" alt="women_politics">
						</div>
					</a>
					<div class="contentWrap">
						<div class="headContent">
							<div class="category">
								<a href="#">
								   World & Politics
								</a>
							    <ul>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
									<li>
										<div class="threeDot">
											<span></span>
											<span></span>
											<span></span>
											<div class="dropdownElement">
												<ul>
													<li><a href="#">Dismiss this story</a></li>
													<li><a href="#">Mute this author</a></li>
													<li><a href="#">Mute this publication</a></li>
												</ul>
											</div>
										</div>
									</li>
							    </ul>
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
							</ul>
						</div>
				    </div>
			     </article>
			</div>

			<div class="col-lg-6 mb-3">
			     <article class="vertical two_way_article">
			     	<a href="#">
						<div class="imgWrap">
							<img src="assets/img/women_politics_article.png" alt="women_politics">
						</div>
					</a>
					<div class="contentWrap">
						<div class="headContent">
							<div class="category">
								<a href="#">
								   World & Politics
								</a>
							    <ul>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
									<li>
										<div class="threeDot">
											<span></span>
											<span></span>
											<span></span>
											<div class="dropdownElement">
												<ul>
													<li><a href="#">Dismiss this story</a></li>
													<li><a href="#">Mute this author</a></li>
													<li><a href="#">Mute this publication</a></li>
												</ul>
											</div>
										</div>
									</li>
							    </ul>
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
							</ul>
						</div>
				    </div>
			     </article>
			</div>

			<div class="col-lg-6 mb-3">
			     <article class="vertical two_way_article">
			     	<a href="#">
						<div class="imgWrap">
							<img src="assets/img/women_politics_article.png" alt="women_politics">
						</div>
					</a>
					<div class="contentWrap">
						<div class="headContent">
							<div class="category">
								<a href="#">
								   World & Politics
								</a>
							    <ul>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
									<li>
										<div class="threeDot">
											<span></span>
											<span></span>
											<span></span>
											<div class="dropdownElement">
												<ul>
													<li><a href="#">Dismiss this story</a></li>
													<li><a href="#">Mute this author</a></li>
													<li><a href="#">Mute this publication</a></li>
												</ul>
											</div>
										</div>
									</li>
							    </ul>
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
							</ul>
						</div>
				    </div>
			     </article>
			</div>

			<div class="col-lg-6 mb-3">
			     <article class="vertical two_way_article">
			     	<a href="#">
						<div class="imgWrap">
							<img src="assets/img/women_politics_article.png" alt="women_politics">
						</div>
					</a>
					<div class="contentWrap">
						<div class="headContent">
							<div class="category">
								<a href="#">
								   World & Politics
								</a>
							    <ul>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
									<li>
										<div class="threeDot">
											<span></span>
											<span></span>
											<span></span>
											<div class="dropdownElement">
												<ul>
													<li><a href="#">Dismiss this story</a></li>
													<li><a href="#">Mute this author</a></li>
													<li><a href="#">Mute this publication</a></li>
												</ul>
											</div>
										</div>
									</li>
							    </ul>
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
							</ul>
						</div>
				    </div>
			     </article>
			</div>

			<div class="col-lg-6 mb-3">
			     <article class="vertical two_way_article">
			     	<a href="#">
						<div class="imgWrap">
							<img src="assets/img/women_politics_article.png" alt="women_politics">
						</div>
					</a>
					<div class="contentWrap">
						<div class="headContent">
							<div class="category">
								<a href="#">
								   World & Politics
								</a>
							    <ul>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
									<li>
										<div class="threeDot">
											<span></span>
											<span></span>
											<span></span>
											<div class="dropdownElement">
												<ul>
													<li><a href="#">Dismiss this story</a></li>
													<li><a href="#">Mute this author</a></li>
													<li><a href="#">Mute this publication</a></li>
												</ul>
											</div>
										</div>
									</li>
							    </ul>
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
							</ul>
						</div>
				    </div>
			     </article>
			</div>

			<div class="col-lg-6 mb-3">
			     <article class="vertical two_way_article">
			     	<a href="#">
						<div class="imgWrap">
							<img src="assets/img/women_politics_article.png" alt="women_politics">
						</div>
					</a>
					<div class="contentWrap">
						<div class="headContent">
							<div class="category">
								<a href="#">
								   World & Politics
								</a>
							    <ul>
									<li class="bookmarkArticle"><a href="#" title="Bookmark the article"><svg class="icon icon-save-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-save-icon"></use></svg></a></li>
									<li class="shareArticle"><a href="#" title="Share the article"><svg class="icon icon-share"><use xlink:href="assets/img/cobold-sprite.svg#icon-share"></use></svg></a>
	                                <div class="socialShare">
	                                 	<ul>
											<li><a href="#"><img src="assets/img/facebook-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/twitter-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/insta-dark.svg" alt=""></a></li>
											<li><a href="#"><img src="assets/img/linkedin-dark.svg" alt=""></a></li>
										</ul>
	                                 </div>
									</li>
									<li>
										<div class="threeDot">
											<span></span>
											<span></span>
											<span></span>
											<div class="dropdownElement">
												<ul>
													<li><a href="#">Dismiss this story</a></li>
													<li><a href="#">Mute this author</a></li>
													<li><a href="#">Mute this publication</a></li>
												</ul>
											</div>
										</div>
									</li>
							    </ul>
							</div>
							<a href="#">
							<h3>The absolutely remarkable social power of Alexandria Ocasio-Cortez.</h3>
							</a>
						</div>
						<div class="authorDetails">
							<ul>
								<li>By Shelly Mathur</li>
								<li>4 Hours Ago</li>
							</ul>
						</div>
				    </div>
			     </article>
			</div>
		</div>
		<div class="pagination">
			<ul>
				<li class="leftBtn"><a href="#"><img src="assets/img/leftarw.svg" alt="left-arrow"></a></li>
				<li><span>Page 1/100 </span></li>
				<li class="rightBtn"><a href="#"><img src="assets/img/rightarw.svg" alt="right-arrow"></a></li>
			</ul>
		</div>
	</div>
</section>
   
<!-- Explore Stories -->
<section class="Section ExploreStories fullWidth">
	<div class="container">
		<div class="Wrapper">
			<div class="storyLinkWrapper">
				<div class="StoryLink">
					<h2>Explore more stories of inspiration and trailblazing</h2>
					<p>Select the topics, subjects and blogs that interests you the most.</p>
					<ul>
						<li><a href="#">World & Politics</a></li>
						<li><a href="#">Self & Wellness</a></li>
						<li><a href="#">Arts & Culture</a></li>
						<li><a href="#">Beauty & Fitness</a></li>
						<li><a href="#">Gender & Identity</a></li>
						<li><a href="#">Science & Technology</a></li>
						<li><a href="#">Design & Fashion</a></li>
						<li><a href="#">Books & Publications</a></li>
						<li><a href="#">Artist & entertainment</a></li>
						<li><a href="#">Social & Society</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>


<?php @include('template-parts/footer.php'); ?>