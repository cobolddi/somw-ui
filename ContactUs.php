<?php @include('template-parts/header.php') ?>

<section class="ContactUsPage Section">
	<div class="container">
		<div class="TopHeading">
			<h2>Contact The Society</h2>
			<p>Get in touch with us and send us your enquiries by dropping us a line.</p>
		</div>
		<div class="ContactDetails">
			<div class="row">
				<div class="col-12 col-md-6">
					<div class="Details">
						<div class="IconBox">
							<img src="assets/img/border-mail.svg" alt="">
						</div>
						<div class="DetailsBox">
							<h4>Email Us</h4>
							<p>For all detailed queries, reach us through email: </p>
							<p><a href="mailto:contact@somw.com">contact@somw.com</a></p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Details">
						<div class="IconBox">
							<img src="assets/img/bordertwitter.svg" alt="">
						</div>
						<div class="DetailsBox">
							<h4>Tweet at us</h4>
							<p>For short questions, you can tweet us</p> 
							<p><a href="#">@SOMWsupport.</a></p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Details">
						<div class="IconBox">
							<img src="assets/img/border-fb.svg" alt="">
						</div>
						<div class="DetailsBox">
							<h4>Find Us On Facebook</h4>
							<p>You can drop us a message on Facebook :</p>
							<p><a href="#">SOMW Facebook Page</a></p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-6">
					<div class="Details">
						<div class="IconBox">
							<img src="assets/img/border-announce.svg" alt="">
						</div>
						<div class="DetailsBox">
							<h4>Advertise with us</h4>
							<p>To explore advertising and partnership opportunities,write us at:</p>
							<p><a href="mailto:advertise@somw.com">advertise@somw.com</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
        	
		<div class="whiteBoxForm">
			<div class="formHeading">
				<h2>Submit A Request</h2>
				<p>Fill in the form below to submit a Quick a query</p>
			</div>
			<form action="">
				<div class="row">
					<div class="col-12 col-md-6 MBottom2">
						<label>First Name</label>
						<input type="text">
					</div>
					<div class="col-12 col-md-6 MBottom2">
						<label>Emaill address</label>
						<input type="email">
					</div>
					<div class="col-12 col-md-6 MBottom2">
						<label>Query Subject (max 50 words)</label>
						<input type="text">
					</div>
					<div class="col-12 col-md-6 MBottom2">
						<label>Country</label>
						<select name="" id="">
							<option selected disabled>Select Country</option>
							<option value="">A</option>
							<option value="">B</option>
						</select>
					</div>
					<div class="col-12 col-md-12 MBottom2">
						<label>Your Message</label>
						<textarea></textarea>
					</div>
					<div class="col-12 col-md-3 mb-0">
						<input type="submit" value="submit">
					</div>
				</div>
			</form>
		</div>

	</div>
</section>

<?php @include('template-parts/footer.php') ?>
