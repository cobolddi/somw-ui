<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Society of Modern Women</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/favicon.png">
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>

<body>


		
<header class="DesktopOnly" id="header">
	<!-- <div class="TopHeader">
		<div class="container">
			<p>Some special notices and highlighted content can scroll over here</p>
		</div>
	</div>	 -->
	<div class="NavigationBlock">
		<div class="container">
			<div class="row alignItemCenter">
				<div class="col-lg-4 col-md-3">
					<div class="HeaderLogo">
						<a href="index.php" class="StillLogo"><img src="assets/img/logo.svg" alt=""></a>
						<a href="index.php" class="ScrollLogo"><img src="assets/img/onlylogo.svg" alt=""></a>
						<a href="index.php" class="StillLogo"><img src="assets/img/logo_white.svg" alt=""></a>
					</div>
				</div>
				<div class="col-lg-8 col-md-9">
					<div class="TopMenu">
						<ul>
							<li><a href="#">About</a></li>
							<li><a href="guideLine.php">Write for us</a></li>
							<!-- <li><a href="ContactUs.php">Contact us</a></li> -->
							<li><a href="ContactUs.php">Join the Society</a></li>
							<li><a href="#signInPopUp" class="signInBtn">Login</a></li>
						</ul>
                        
                        <div class="topbarNav">
							<ul>	
								<li>
								<svg class="icon icon-day-mode-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-day-mode-icon"></use></svg>
									<label class="switch">
									     <input type="checkbox">
									     <span class="slider round"></span>
									</label>
								<svg class="icon icon-night-mode"><use xlink:href="assets/img/cobold-sprite.svg#icon-night-mode"></use></svg>
								</li>

								<li><a href="#"><svg class="icon icon-bell-notify"><use xlink:href="assets/img/cobold-sprite.svg#icon-bell-notify"></use></svg></a></li>

								<li><a href="#"><svg class="icon icon-bookmark-count"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark-count"></use></svg></a></li>

								<li><a href="#searchBar"><svg class="icon icon-search"><use xlink:href="assets/img/cobold-sprite.svg#icon-search"></use></svg></a></li>

								<li><a href="#ProfileProgress" class="profilePopup"><span class="profile-icon">AK</span></a></li>
							</ul>
						</div>
					</div>
					<div class="MainMenu">
						<ul>
							<li class="submenu"><a href="category-page.php">World & Politics</a>
							</li>
							<li><a href="#">Self & Wellness</a></li>
							<li><a href="#">Art & Culture</a></li>
							<li><a href="#">Cuisine & Health</a></li>
							<li class="submenu">
								<a href="#">All</a>
								   <!-- <span class="crossSign" id="subMenuClose">&times;</span>	 -->
								  <ul>
								  	<li><a href="#">Self & Wellness</a>
								  		<ul>
									  	<li><a href="#">Self Care</a></li>
									  	<li><a href="#">Mind & body</a></li>
									  	<li><a href="#">Health</a></li>
									  	<li><a href="#">Lifestyle</a></li>
									  	</ul>
								    </li>

								  	<li><a href="#">Science and Technology</a>
								  	   <ul>
									  	<li><a href="#">Tech</a></li>
									  	<li><a href="#">Trade</a></li>
									  	<li><a href="#">Innoovation</a></li>
									  	<li><a href="#">Education</a></li>
								  	  </ul>
								    </li>
								  


								  	<li><a href="#">Arts & Culture</a>
								  	  <ul>
									  	<li><a href="#">Artworks</a></li>
									  	<li><a href="#">Collections</a></li>
									  	<li><a href="#">Influence</a></li>
									  	<li><a href="#">creations</a></li>
									  </ul>
									</li>
								  


								  	<li><a href="#">Design & Fashion</a>
								  		<ul>
									  	<li><a href="#">Artworks</a></li>
									  	<li><a href="#">Collections</a></li>
									  	<li><a href="#">Influence</a></li>
									  	<li><a href="#">creations</a></li>
									  	</ul>
								    </li>
								  


								  	<li><a href="#">Beauty & Fitness</a>
								  		<ul>
									  	<li><a href="#">Products</a></li>
									  	<li><a href="#">Personal Care</a></li>
									  	<li><a href="#">Balanced Eating</a></li>
									  	<li><a href="#">Routines</a></li>
									  	</ul>
								  	</li>
								  


								  	<li><a href="#">Artist and Entertainment</a>
								  		<ul>
									  	<li><a href="#">Products</a></li>
									  	<li><a href="#">Personal Care</a></li>
									  	<li><a href="#">Balanced Eating</a></li>
									  	<li><a href="#">Routines</a></li>
									  	</ul>
								  	</li>
								  

								  	<li><a href="#">Identity</a>
								  		<ul>
									  	<li><a href="#">LGBTQ</a></li>
									  	<li><a href="#">Gender</a></li>
									  	<li><a href="#">Representation</a></li>
									  	<li><a href="#">Community</a></li>
									  	</ul>
								  	</li>
								  
								  	<li><a href="#">Books & Publication</a>
								  		<ul>
									  	<li><a href="#">LGBTQ</a></li>
									  	<li><a href="#">Gender</a></li>
									  	<li><a href="#">Representation</a></li>
									  	<li><a href="#">Community</a></li>
									  	</ul>
								  	</li>
								  </ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>

<header class="MobileOnly">
	<!-- <div class="TopHeader">
		<div class="container">
			<div class="ticker">
				<p class="tickertext">Some special notices and highlighted content can scroll over here</p>
			</div>
		</div>
	</div> -->    

    <div class="mobile_top_nav">
		<ul>
			<li>
			<svg class="icon icon-day-mode-icon"><use xlink:href="assets/img/cobold-sprite.svg#icon-day-mode-icon"></use></svg>
				<label class="switch">
				     <input type="checkbox">
				     <span class="slider round"></span>
				</label>
			<svg class="icon icon-night-mode"><use xlink:href="assets/img/cobold-sprite.svg#icon-night-mode"></use></svg>
			</li>
			<li><a href=""><svg class="icon icon-bell-notify"><use xlink:href="assets/img/cobold-sprite.svg#icon-bell-notify"></use></svg></a></li>
			<li><a href=""><svg class="icon icon-bookmark-count"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark-count"></use></svg></a></li>
			<li><a href="#searchBar"><svg class="icon icon-search"><use xlink:href="assets/img/cobold-sprite.svg#icon-search"></use></svg></a></li>
			<li><a href="#signInPopUp" class="signInBtn">Login</a></li>
			<li><a href="#ProfileProgress" class="profilePopup"><span class="profile-icon">AK</span></a></li>
		</ul>
	</div>
	<div class="MobileMenu">
		<div class="menuOverlay"></div>
		<div class="HeaderLogo">
			<a href="index.php" class="StillLogo"><img src="assets/img/logo.svg" alt=""></a>
			<a href="index.php" class="ScrollLogo"><img src="assets/img/onlylogo.svg" alt=""></a>
			<a href="index.php" class="StillLogo"><img src="assets/img/logo_white.svg" alt=""></a>
		</div>
		<div class="MenuButton">
			<a href="#" class="menuBtn">
				<span class="lines"></span>
			</a>
		</div>
		<nav class="mainMenu">
			<!-- <a href="#" class="closeMenu">&times;</a> -->
			<ul>
				<li>
					<a href="index.php">Home</a>
				</li>
				<li>
					<a href="#">About</a>
				</li>
				<li>
					<a href="guideLine.php">Write for us</a>
				</li>
				<li>
					<a href="ContactUs.php">Join the Society</a>
				</li>
			</ul>

			<h4>Category</h4>
			<ul class="categoryMenu">
				<li>
					<a href="#">Self & Wellness</a>
					<ul class="subCat_nav">
				  	<li><a href="#">Self Care</a></li>
				  	<li><a href="#">Mind & body</a></li>
				  	<li><a href="#">Health</a></li>
				  	<li><a href="#">Lifestyle</a></li>
				  </ul>
				</li>
				<li>
					<a href="#">Science and Technology</a>
					  <ul class="subCat_nav">
					  	<li><a href="#">Tech</a></li>
					  	<li><a href="#">Trade</a></li>
					  	<li><a href="#">Innoovation</a></li>
					  	<li><a href="#">Education</a></li>
					  </ul>
				</li>
				<li>
					<a href="#">Arts & Culture</a>
					  <ul class="subCat_nav">
					  	<li><a href="#">Artworks</a></li>
					  	<li><a href="#">Collections</a></li>
					  	<li><a href="#">Influence</a></li>
					  	<li><a href="#">creations</a></li>
					  </ul>
				</li>
				<li>
					<a href="#">Design & Fashion</a>
					 <ul class="subCat_nav">
					  	<li><a href="#">Artworks</a></li>
					  	<li><a href="#">Collections</a></li>
					  	<li><a href="#">Influence</a></li>
					  	<li><a href="#">creations</a></li>
					  </ul>
				</li>

				<li>
					<a href="#">Beauty & Fitness</a>
					 <ul class="subCat_nav">
					  	<li><a href="#">Products</a></li>
					  	<li><a href="#">Personal Care</a></li>
					  	<li><a href="#">Balanced Eating</a></li>
					  	<li><a href="#">Routines</a></li>
					  </ul>
				</li>

				<li>
					<a href="#">Artist & Entertainment</a>
					 <ul class="subCat_nav">
					  	<li><a href="#">Products</a></li>
					  	<li><a href="#">Personal Care</a></li>
					  	<li><a href="#">Balanced Eating</a></li>
					  	<li><a href="#">Routines</a></li>
					  </ul>
				</li>

			</ul>
		</nav>
	</div>
</header>

<main class="shrink">
	
	
	
	
