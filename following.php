
<?php @include('template-parts/header.php'); ?>

<section class="MyProfile Section">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-3">
				<div class="sideNavbar">
					<h2 class="userProfileNav"><span>User Profile</span> <img src="assets/img/chevron-down-white.svg" alt="dropdown_arrow"></h2>
                   <ul id="sideNav">
                     <li class="nav-item">
                     	<svg class="icon icon-user"><use xlink:href="assets/img/cobold-sprite.svg#icon-user"></use></svg>
                       <a href="myProfile.php">My Profile</a>
                     </li>
                     <li class="nav-item active">
                     	<svg class="icon icon-following"><use xlink:href="assets/img/cobold-sprite.svg#icon-following"></use></svg>
                       <a href="#">Following</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-bookmark-count"><use xlink:href="assets/img/cobold-sprite.svg#icon-bookmark-count"></use></svg>
                       <a href="savedArticles.php">Saved Articles</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-my_quotes"><use xlink:href="assets/img/cobold-sprite.svg#icon-my_quotes"></use></svg>
                       <a href="myQuotes.php">My Quotes</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-files"><use xlink:href="assets/img/cobold-sprite.svg#icon-files"></use></svg>
                       <a href="myArticles.php">My Articles</a>
                     </li>
                     <li class="nav-item">
                     	<svg class="icon icon-logout"><use xlink:href="assets/img/cobold-sprite.svg#icon-logout"></use></svg>
                       <a href="#">Logout</a>
                     </li>
                   </ul>
                </div>
			</div>

			<div class="col-lg-9">
				<div class="contentWrap">
					<div class="heading">
						<h3>Following</h3>
					</div>
					<div class="innerWrap">
						<div class="list">
							<ul>
								<li>
									<div class="leftSide mb-2-sm">
										<a href="#">
											<div class="imgWrap">
												<img src="assets/img/Photo.png" alt="photo">
											</div>
											<div class="userDetail">
												<h4>Aarushi Sharma</h4>
												<span>June 6, 2020</span>
											</div>
										</a>
									</div>
									<div class="rightSide">
										<select>
											<option>Following</option>
											<option>Unfollow</option>
										</select>
									</div>
								</li>

								<li>
									<div class="leftSide mb-2-sm">
										<a href="#">
											<div class="imgWrap">
												<img src="assets/img/Photo.png" alt="photo">
											</div>
											<div class="userDetail">
												<h4>Aarushi Sharma</h4>
												<span>June 6, 2020</span>
											</div>
										</a>
									</div>
									<div class="rightSide">
										<select>
											<option>Following</option>
											<option>Unfollow</option>
										</select>
									</div>
								</li>

								<li>
									<div class="leftSide mb-2-sm">
										<a href="#">
											<div class="imgWrap">
												<img src="assets/img/Photo.png" alt="photo">
											</div>
											<div class="userDetail">
												<h4>Aarushi Sharma</h4>
												<span>June 6, 2020</span>
											</div>
										</a>
									</div>
									<div class="rightSide">
										<select>
											<option>Following</option>
											<option>Unfollow</option>
										</select>
									</div>
								</li>

								<li>
									<div class="leftSide mb-2-sm">
										<a href="#">
											<div class="imgWrap">
												<img src="assets/img/Photo.png" alt="photo">
											</div>
											<div class="userDetail">
												<h4>Aarushi Sharma</h4>
												<span>June 6, 2020</span>
											</div>
										</a>
									</div>
									<div class="rightSide">
										<select>
											<option>Following</option>
											<option>Unfollow</option>
										</select>
									</div>
								</li>

								<li>
									<div class="leftSide mb-2-sm">
										<a href="#">
											<div class="imgWrap">
												<img src="assets/img/Photo.png" alt="photo">
											</div>
											<div class="userDetail">
												<h4>Aarushi Sharma</h4>
												<span>June 6, 2020</span>
											</div>
										</a>
									</div>
									<div class="rightSide">
										<select>
											<option>Following</option>
											<option>Unfollow</option>
										</select>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php'); ?>
	
	
