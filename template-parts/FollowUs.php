<section class="Section FollowUs">
	<div class="container">
		<div class="Wrapper">
			<div class="row">
				<div class="col-12 col-lg-6">
					<div class="HeadingBox">
						<div class="TopHeading">
						 <h2>Follow us</h2>
						</div>
						<div>
						<h5>Never miss another great story!</h5>
						<p>Get notified of the best stories on the internet.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-lg-6">
					<ul>
						<li><a href="#"><img src="assets/img/facebook.svg" alt=""><br>Facebook</a></li>
						<li><a href="#"><img src="assets/img/twitter.svg" alt=""><br>Twitter</a></li>
						<li><a href="#"><img src="assets/img/insta.svg" alt=""><br>Instagram</a></li>
						<li><a href="#"><img src="assets/img/linkedin.svg" alt=""><br>Linkedin</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>